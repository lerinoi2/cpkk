<?php
function getCity($id, $type = 1)
{
    CModule::IncludeModule('iblock');
    $city = false;
    if ($type == 1) {
        $rs = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 3, 'ACTIVE' => 'Y', 'ID' => $id), false, false, array('ID', 'NAME', 'PROPERTY_NAME_R', 'CODE'));
    }
    else {
        $rs = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 3, 'ACTIVE' => 'Y', '=CODE' => $id), false, false, array('ID', 'NAME', 'PROPERTY_NAME_R', 'CODE'));
    }
    if ($ar = $rs->Fetch()) {
        $city = $ar;
        $city['NAME_R'] = $ar['PROPERTY_NAME_R_VALUE'];
    }
    return $city;
}

AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CUserTypeSectionLink", "GetUserTypeDescription"));

class CUserTypeSectionLink
{

    private static $Script_included = false;

    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "SECTION_LINK",
            "CLASS_NAME" => "CUserTypeSectionLink",
            "DESCRIPTION" => "Ссылка на раздел",
            "BASE_TYPE" => "string",
            "PROPERTY_TYPE" => "N",
            "USER_TYPE" => "SECTION_LINK",
            "GetPublicViewHTML" => array("CUserTypeSectionLink","GetPublicViewHTML"),
            "GetPropertyFieldHtml" => array("CUserTypeSectionLink","GetPropertyFieldHtml"),
        );
    }

    public static function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return $value['VALUE'];
    }

    public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {

        $sectionsList=array();
        $ibNames=array();

        $rsIB = CIBlock::GetList();
        while ($ib = $rsIB->GetNext()) {
            $ibNames[$ib['ID']] = $ib['NAME'];
        }

        $rsBindValues = CIBlockSection::GetTreeList(
            array('ACTIVE' => 'Y'),
            array()
        );
        while ($bind_value = $rsBindValues->Fetch()) {
            $sectionsList[$bind_value['IBLOCK_ID']]['NAME'] = $ibNames[$bind_value['IBLOCK_ID']];
            $sectionsList[$bind_value['IBLOCK_ID']]['SECTIONS'][] = $bind_value;
        }

        $optionsHTML='<option value="">Указать раздел</option>';

        foreach($sectionsList as $ib){

            $optionsHTML .= '<optgroup label="'.$ib['NAME'].'">';

            foreach($ib['SECTIONS'] as $s){
                $optionsHTML .= '<option value="'.$s["ID"].'"'.
                    ( $value["VALUE"]==$s['ID'] ? ' selected' : '' ).
                    '>'.
                    str_repeat("-", $s["DEPTH_LEVEL"])." ".$s['NAME'].' ['.$s['ID'].']'.
                    '</option>';
            }

            $optionsHTML .= '</optgroup>';

        }

        return  '<select name="'.$strHTMLControlName["VALUE"].'" style="display: block;width: 80%;height: 25px;margin: 5px 5px 5px 5px;box-sizing: border-box;"">'.$optionsHTML.'</select>';

    }

}


function genPdfThumbnail($source, $target)
{
    $target = dirname($source).DIRECTORY_SEPARATOR.$target;
    $im     = new imagick($source."[0]"); // 0-first page, 1-second page
    $im->setImageFormat('jpg');
    $im->thumbnailimage(360, 0); // width and height
    $im->writeimage($target);
    $im->clear();
    $im->destroy();
}