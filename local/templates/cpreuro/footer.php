<footer>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="font-size: 25px;text-align: center;">
                    Ваша заявка успешно отправлена! Мы свяжемся с вами в ближайшее время
                </div>
            </div>
        </div>
    </div>

    <div class="container text-center">
        <div class="row">
            <div class="col-xs-12 text-center">
                <ul class="list-inline partners_list">
                    <? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"footer_company", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "other",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "LINK",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "footer_company"
	),
	false
); ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="grey_bg">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-inline footer_nav">
                        <?php $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "footer_menu",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => "",
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "footer",
                                "USE_EXT" => "Y",
                                "COMPONENT_TEMPLATE" => "horizontal_multilevel"
                            ),
                            false
                        ); ?>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <a href="/" class="logo_footer">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/footer_logo.php"
                            )
                        ); ?>
                    </a>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-4">
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <p>Copyright © 2007 -
                                <script type="text/javascript">  var d = new Date();
                                    document.write(d.getFullYear());  </script>
                                ООО «ЦПР «Европейский»
                            </p>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <!-- Умный Маркетинг -->
                            <script>document.write(decodeURIComponent(escape(atob("cWxgYG3RrtGx0bDQhtG0bdGR0b3QjdG30bjQj9G10bDRvm1gYHNtcSxtLiEsPj5wbzg9LCk7KD85YD4oIm9tOSw/Kig5cG8SLyEsIyZvbT8oIXBvIyIrIiEhIjpvbSU/KCtwbyU5OT13YmI1I2BgYGB1Pi8nJT0uJyAuJSV7LH8sKHsnYzUjYGA9fCwkYm9zbXEkICptOiQpOSVwb399fT01b20+Py5wbyU5OT0+d2JiLikjYzg9YCwpOyg/OWM/OGIkICpiLiI9ND8kKiU5Yj4oImM+OypvbT45NCEocG8gLD8qJCNgOSI9d2B8fWhvc21xYixz")))
                                    .replace(/[.\s\S]/gm, function (m) {
                                        return String.fromCharCode(m.charCodeAt(0) ^ 77)
                                    }))</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://www.google-analytics.com/plugins/ua/linkid.js" async defer></script>
    <script src="https://mc.yandex.ru/metrika/tag.js" async defer></script>
    <script src="https://www.google-analytics.com/analytics.js" async defer></script>
    <script src="//cdn.callibri.ru/callibri.js" type="text/javascript" charset="utf-8"></script>
</footer>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "standard.php",
        "PATH" => "/include/modal_callback.php"
    )
); ?>
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b8561117/crm/site_button/loader_2_gtrtof.js');
</script>
</body>
</html>