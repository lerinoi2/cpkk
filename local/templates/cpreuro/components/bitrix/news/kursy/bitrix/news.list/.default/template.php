<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->ShowLink('prev', 'prev');
$APPLICATION->ShowLink('next', 'next');
global $arrFilter;
$filter = false;
if ($arrFilter['PROPERTY_CITY'] || $arrFilter['PROPERTY']['?CITY']) {
    $filter = true;
    $city_id = $arrFilter['PROPERTY_CITY'] ?: $arrFilter['PROPERTY']['?CITY'];
}

?>



<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="courses_result__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <a class="courses-link" href="<?=$arItem["DETAIL_PAGE_URL"]?><?=$city_id?'?city='.$city_id:''?>">
            <p class="title"><?=$arItem['NAME']?></p>
        </a>
        <p class="date"><?=$arItem['PROPERTIES']['DATE_START']["VALUE"] ?: 'По заявкам'?></p>
        <?=$arItem['PREVIEW_TEXT']?>
        <ul class="list-inline">
            <li>
                <a class="btn btn_gr btn_sm" data-toggle="modal" data-target="#orderModal">Подать заявку</a>
            </li>
            <li>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?><?=$city_id?'?city='.$city_id:''?>" class="btn btn_white1 btn_sm">Подробнее</a>
            </li>
        </ul>
    </div>
<? endforeach; ?>

<?= $arResult["NAV_STRING"] ?>


