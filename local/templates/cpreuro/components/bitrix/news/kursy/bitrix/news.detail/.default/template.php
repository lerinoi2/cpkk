<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="course">
    <div class="container">
        <div class="course__container">
            <div class="course__left">
                <div class="course__breadcrumbs">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "bread",
                        array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0"
                        )
                    ); ?>
                </div>
                <div class="course__title">
                    <h1><?= $arResult['NAME'] ?></h1>
                </div>
                <div class="course__text">
                    <?= $arResult["DETAIL_TEXT"] ?>
                </div>
                <div class="course__button">
                    <button class="btn-blue btn-md" data-toggle="modal" data-target="#orderModal">
                        Заявка на обучение
                    </button>
                </div>
            </div>
            <div class="course__right">

                <?
                $defimg = '';
                if (!$arResult['DISPLAY_PROPERTIES']['IMG_FOR_BIG_TEXT']['FILE_VALUE']['SRC']) {
                    $db_list = CIBlockSection::GetList(array(), $arFilter = array("IBLOCK_ID" => 4, "ID" => $arResult['IBLOCK_SECTION_ID']), true,
                        $arSelect = array("UF_*"));
                    while ($ar_result = $db_list->GetNext()) {
                        $defimg = CFile::GetPath($ar_result['UF_DEFIMG']) ?: '/img/course/course-1.jpg';
                    }
                }
                ?>

                <img src="<?= $arResult['DISPLAY_PROPERTIES']['IMG_FOR_BIG_TEXT']['FILE_VALUE']['SRC'] ?: $defimg ?>" alt="">
            </div>
        </div>
        <div class="course__questions">
            <? if ($arResult['DISPLAY_PROPERTIES']['BIG_TEXT']['VALUE']['TEXT']): ?>
                <div class="question_v2 training">
                    <div class="question_v2__title">программа обучения</div>
                    <div class="question_v2__text">
                        <?= html_entity_decode($arResult['DISPLAY_PROPERTIES']['BIG_TEXT']['VALUE']['TEXT']) ?>
                    </div>
                    <? if ($arResult['DISPLAY_PROPERTIES']['PROP1']['VALUE'] || $arResult['DISPLAY_PROPERTIES']['PROP2']['VALUE'] || $arResult['DISPLAY_PROPERTIES']['PROP3']['VALUE'] || $arResult['DISPLAY_PROPERTIES']['PROP4']['VALUE']): ?>
                        <div class="question_v2__more _moreInfo" data-text="Подробнее">Подробнее</div>
                    <? endif; ?>
                    <div class="question_v2__description">
                        <div class="training__info">
                            <? if ($arResult['DISPLAY_PROPERTIES']['PROP1']['VALUE']): ?>
                                <span>
                            <b>Форма обучения:</b> <?= $arResult['DISPLAY_PROPERTIES']['PROP1']['VALUE'] ?>
                        </span>
                            <? endif; ?>
                            <? if ($arResult['DISPLAY_PROPERTIES']['PROP2']['VALUE']): ?>
                                <span>
                            <b>Продолжительность обучения:</b> <?= $arResult['DISPLAY_PROPERTIES']['PROP2']['VALUE'] ?>
                        </span>
                            <? endif; ?>
                            <? if ($arResult['DISPLAY_PROPERTIES']['PROP3']['VALUE']): ?>
                                <span>
                            <b>Требования:</b> <?= $arResult['DISPLAY_PROPERTIES']['PROP3']['VALUE'] ?>
                        </span>
                            <? endif; ?>
                        </div>
                        <? if ($arResult['DISPLAY_PROPERTIES']['PROP4']['VALUE']): ?>
                            <div class="training__text">
                                <? $sum = 0; ?>
                                <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP4']['VALUE'] as $l => $time): ?>
                                    <p><?= $l + 1 ?>. <?= $time ?>: <b><?= $arResult['DISPLAY_PROPERTIES']['PROP4']['DESCRIPTION'][$l] ?> ч.</b></p>
                                    <? $sum += $arResult['DISPLAY_PROPERTIES']['PROP4']['DESCRIPTION'][$l]; ?>
                                <? endforeach; ?>
                            </div>
                            <div class="training__result">Итого: <?= $sum ?> ч.</div>
                        <? endif; ?>
                    </div>
                </div>
            <? endif; ?>
            <div class="question_v2 docs">
                <div class="question_v2__title">Необходимые документы</div>
                <div class="question_v2__more _moreInfo" data-text="Подробнее">Подробнее</div>
                <div class="question_v2__description">
                    <div class="docs__text">
                        <? if ($arResult['DISPLAY_PROPERTIES']['PROP5']['VALUE']['TEXT']): ?>
                            <?= html_entity_decode($arResult['DISPLAY_PROPERTIES']['PROP5']['VALUE']['TEXT']) ?>
                        <? else: ?>
                            <p>
                                1. Паспорт
                            </p>
                            <p>
                                2. СНИЛС
                            </p>
                            <p>
                                3. Копия диплома о высшем или среднем профессиональном образовании (для проф. переподготовки)
                            </p>
                            <p>
                                4. <a href="/upload/Согласие на обработку персональных данных для слушателей курсов.doc" style="color: #008EF0;">Согласие на
                                    обработку персональных
                                    данных*</a>
                            </p>
                            <br>
                            <p>
                                * желтые поля обязательны для заполнения для всех курсов
                            </p>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="course__form">
            <div class="course__application">
                <div class="course__block-title">Оставьте заявку на курс</div>
                <form action="" class="form" id="orderkurs">
                    <div class="form__block">
                        <div class="form__title">Форма обучения</div>
                        <div class="form__inputs">
                            <? if ($arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                                <div class="form__radio">
                                    <div class="radioButton">
                                        <input type="radio" id="radio-1" name="typeFormat" checked>
                                        <label for="radio-1">Очное обучение</label>
                                    </div>
                                </div>
                            <? endif; ?>

                            <? if ($arResult['PROPERTIES']['DISTANCE']['VALUE'] === 'Да'): ?>
                                <div class="form__radio">
                                    <div class="radioButton">
                                        <input type="radio" id="radio-2" name="typeFormat"<?= !$arResult['PROPERTIES']['HOURS']['VALUE'] ? 'checked' : '' ?>>
                                        <label for="radio-2">Дистанционное обучение</label>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>


                    <div class="form__block">
                        <div class="city_choice" style="<?= !$arResult['PROPERTIES']['HOURS']['VALUE'] ? 'display:none' : '' ?>">
                            <div class="form__title">Выберите город</div>
                            <div class="select">
                                <div class="select__selected _chooseSelect">
                                    <span><?= $arResult['cities'][0] ?></span>
                                </div>

                                <div class="select__items _customScroll">

                                    <? foreach ($arResult['cities'] as $q => $item): ?>
                                        <div value="<?= $q ?>" class="select__item <?= $q === 0 ? 'active' : '' ?>"><?= $item ?></div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>


                        <? if (!$arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                            <div class="form__price active">СТОИМОСТЬ КУРСА: <?= $arResult['PROPERTIES']['PRICE_DIS']['VALUE'] ?> руб.
                                <p style="color: black;font-size: 14px;font-weight: 100;">Образовательные услуги НДС не облагаются на основании п.п.14 п.2 ст
                                    .149 НК РФ</p>
                            </div>
                        <? endif; ?>

                        <? if ($arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                            <? $n_c = 0; ?>
                            <? foreach ($arResult['PROPERTIES']['PRICE']['NEWVALUE'] as $key => $price): ?>
                                <? if ($key === $arResult['cities'][0]): ?>
                                    <div class="form__price active <?= $n_c ?> och">СТОИМОСТЬ КУРСА: <?= $price ?> руб.
                                        <p style="color: black;font-size: 14px;font-weight: 100;">Образовательные услуги НДС не облагаются на основании п.п
                                            .14 п.2 ст.149 НК
                                            РФ</p>
                                    </div>
                                <? else: ?>
                                    <div class="form__price <?= $n_c ?>">СТОИМОСТЬ КУРСА: <?= $price ?> руб.
                                        <p style="color: black;font-size: 14px;font-weight: 100;">Образовательные услуги НДС не облагаются на основании п.п
                                            .14 п.2 ст.149 НК
                                            РФ</p>
                                    </div>
                                <? endif; ?>
                                <? $n_c++; ?>
                            <? endforeach; ?>
                            <div class="form__price dist">СТОИМОСТЬ КУРСА:<?= $arResult['PROPERTIES']['PRICE_DIS']['VALUE'] ?> руб.
                                <p style="color: black;font-size: 14px;font-weight: 100;">Образовательные услуги НДС не облагаются на основании п.п.14 п.2 ст
                                    .149 НК РФ</p>
                            </div>
                        <? endif; ?>


                        <!--                        <div class="kursi-info-item__text">-->
                        <!--                            --><? // if ($_GET['city']): ?>
                        <!--                                --><? //
                        //                                $key = array_search($_GET['city'], $arResult['PROPERTIES']['PRICE_NEW']['DESCRIPTION'], true);
                        //                                $p = $arResult['PROPERTIES']['PRICE_NEW']['VALUE'][$key];
                        //                                if ($p):?>
                        <!--                                    от --><? //= $p ?><!-- руб-->
                        <!--                                --><? // else: ?>
                        <!--                                    от --><? //= $arResult['PROPERTIES']['PRICE']['VALUE'] ?><!-- руб-->
                        <!--                                --><? // endif; ?>
                        <!--                            --><? // else: ?>
                        <!--                                от --><? //= $arResult['PROPERTIES']['PRICE']['VALUE'] ?><!-- руб-->
                        <!--                            --><? // endif; ?>
                        <!--                        </div>-->


                        <div class="form__inputs">
                            <div class="form__input">
                                <input type="text" class="input" placeholder="Ваше имя">
                            </div>
                            <div class="form__input">
                                <input type="tel" class="input" placeholder="Телефон">
                            </div>
                        </div>
                        <div class="form__private">
                            <div class="checkbox">
                                <input type="checkbox" id="private-1" checked="true">
                                <label for="private-1">Согласен на обработку персональных данных </label>
                            </div>
                        </div>
                        <div class="form__btn">
                            <div class="btn-blue btn-md">Отправить заявку</div>
                        </div>
                    </div>
                </form>
            </div>


            <div class="course__block">
                <div class="course__info">
                    <div class="course__block-title">Информация о курсе</div>
                    <div class="kursi-info-list">
                        <? if ($arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/info-ic-2.svg"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Количество<br> часов</div>
                                        <div class="kursi-info-item__text hour_och"><?= $arResult['PROPERTIES']['HOURS']['VALUE'] ?> ч</div>
                                        <div class="kursi-info-item__text hour_dist"><?= $arResult['PROPERTIES']['HOURS_DIS']['VALUE'] ?> ч</div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if (!$arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/info-ic-2.svg"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Количество<br> часов</div>
                                        <div class="kursi-info-item__text"><?= $arResult['PROPERTIES']['HOURS_DIS']['VALUE'] ?> ч</div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['PROP2']['VALUE']): ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/Frame.png"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Продолжи-<br>тельность</div>
                                        <div class="kursi-info-item__text"><?= $arResult['PROPERTIES']['PROP2']['VALUE'] ?></div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['PROP16']['VALUE']): ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/info-ic-1.svg"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Проверка знаний</div>
                                        <div class="kursi-info-item__text"><?= $arResult['PROPERTIES']['PROP16']['VALUE'] ?></div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                        <!--                        <? /* if ($arResult['PROPERTIES']['PRICE']['VALUE']): */ ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/price.svg"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Стоимость обучения</div>
                                        <div class="kursi-info-item__text">
                                            <? /* if ($_GET['city']): */ ?>
                                                <? /*
                                                $key = array_search($_GET['city'], $arResult['PROPERTIES']['PRICE_NEW']['DESCRIPTION'], true);
                                                $p = $arResult['PROPERTIES']['PRICE_NEW']['VALUE'][$key];
                                                if ($p):*/ ?>
                                                    от <? /*= $p */ ?> руб
                                                <? /* else: */ ?>
                                                    от <? /*= $arResult['PROPERTIES']['PRICE']['VALUE'] */ ?> руб
                                                <? /* endif; */ ?>
                                            <? /* else: */ ?>
                                                от <? /*= $arResult['PROPERTIES']['PRICE']['VALUE'] */ ?> руб
                                            <? /* endif; */ ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? /* endif; */ ?>
                        <? /* if (!$arResult['PROPERTIES']['PRICE']['VALUE']): */ ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/price.svg"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Стоимость обучения</div>
                                        <div class="kursi-info-item__text">
                                            <? /* if ($_GET['city']): */ ?>
                                                <? /*
                                                $key = array_search($_GET['city'], $arResult['PROPERTIES']['PRICE_NEW']['DESCRIPTION'], true);
                                                $p = $arResult['PROPERTIES']['PRICE_NEW']['VALUE'][$key];
                                                if ($p):*/ ?>
                                                    от <? /*= $p */ ?> руб
                                                <? /* else: */ ?>
                                                    от <? /*= $arResult['PROPERTIES']['PRICE_DIS']['VALUE'] */ ?> руб
                                                <? /* endif; */ ?>
                                            <? /* else: */ ?>
                                                от <? /*= $arResult['PROPERTIES']['PRICE_DIS']['VALUE'] */ ?> руб
                                            <? /* endif; */ ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        --><? /* endif; */ ?>
                        <? if ($arResult['PROPERTIES']['DOC']['VALUE']): ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/doc.svg"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Итоговые документы</div>
                                        <div class="kursi-info-item__text">
                                            <ul>
                                                <? foreach ($arResult['PROPERTIES']['DOC']['VALUE'] as $doc): ?>
                                                    <li><?= $doc ?></li>
                                                <? endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                        <!--                        <div class="kursi-info-list__item">
                            <div class="kursi-info-item">
                                <div class="kursi-info-item__ic"><img
                                            src="/local/templates/cpreuro/assets/images/info-ic-4.svg"></div>
                                <div class="kursi-info-item__main">
                                    <div class="kursi-info-item__title">Сроки<br> обучения</div>
                                    <div class="kursi-info-item__text">
                                        <? /* if ($_GET['city']): */ ?>
                                            <? /*
                                            if (!$arResult['PROPERTIES']['COUNT_DAY']['VALUE']) {
                                                */ ?>по заявкам<? /*
                                            }
                                            else {
                                                $count_date = 0;
                                                foreach ($arResult['PROPERTIES']['DATE_START_NEW']['DESCRIPTION'] as $k => $c) {
                                                    if ($c === $_GET['city']) {
                                                        $count_date++;
                                                        if ($count_date > 3) {
                                                            break;
                                                        }
                                                        $arDATE_start = ParseDateTime($arResult['PROPERTIES']['DATE_START_NEW']['VALUE'][$k], FORMAT_DATETIME);
                                                        $arDATE_end = ParseDateTime($arResult['PROPERTIES']['DATE_END_NEW']['VALUE'][$k], FORMAT_DATETIME);
                                                        */ ?>
                                                        <div class="time_range">
                                                            <? /*= $arDATE_start["DD"] . " " . ToLower(GetMessage("MONTH_"
                                                                . (int)$arDATE_start["MM"] . "_S")) */ ?>
                                                            - <? /*= $arDATE_end["DD"] . " " . ToLower(GetMessage("MONTH_"
                                                                . (int)$arDATE_end["MM"] . "_S")) */ ?>
                                                        </div>
                                                        <? /*
                                                    }
                                                }
                                            }
                                            */ ?>
                                            <? /* if ($count_date > 3): */ ?>
                                                <a href="#all_date">Смотреть все даты</a>
                                            <? /* elseif ($count_date === 0): */ ?>
                                                по заявкам
                                            <? /* endif; */ ?>
                                        <? /* else: */ ?>
                                            Что бы узнать даты обучения<br>
                                            <a data-toggle="modal" data-target="#callModal_city"
                                               style="cursor: pointer">выберите
                                                город</a>
                                        <? /* endif; */ ?>

                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <!--                        <div class="kursi-info-list__item">
                            <div class="kursi-info-item">
                                <div class="kursi-info-item__ic"><img
                                            src="/local/templates/cpreuro/assets/images/info-ic-5.svg" alt=""></div>
                                <div class="kursi-info-item__main">
                                    <div class="kursi-info-item__title">Место обучения</div>
                                    <div class="kursi-info-item__text">
                                        <? /* if ($_GET['city']): */ ?>
                                            <? /*
                                            $arPlace = [
                                                'Пермь' => 'г. Пермь, бульвар Гагарина, 54а',
                                                'Березники' => 'г. Березники, проезд Сарычева,1',
                                                'Чернушка' => 'г. Чернушка, ул. Коммунистическая, 11',
                                                'Киров' => 'г. Киров, ул. Чапаева, 5, корп. 2',
                                                'Краснокамск' => 'г. Краснокамск, ул. Геофизиков, 14',
                                                'Кунгур' => 'г. Кунгур, ул. Нефтяников, 2',
                                                'Оха' => 'г. Оха, ул. Советская, 26',
                                                'Оса' => 'г. Оса, ул. Степана Разина, 79',
                                                'Полазна' => 'пгт. Полазна, ул. Парковая, 12',
                                                'Усинск' => 'г. Усинск, ул. Нефтяников, 21',
                                            ];
                                            $p = $arPlace[$_GET['city']];
                                            */ ?>
                                            <? /*= $p */ ?>
                                            <br>
                                            <a data-toggle="modal" data-target="#callModal_city"
                                               style="cursor: pointer">Выбрать другой город</a>
                                        <? /* else: */ ?>
                                            Что бы узнать место где проводится обучение<br>
                                            <a data-toggle="modal" data-target="#callModal_city"
                                               style="cursor: pointer">выберите
                                                город</a>
                                        <? /* endif; */ ?>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <!--                        <? /* if ($arResult['PROPERTIES']['DISTANCE']['VALUE'] === 'Да'): */ ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/dis.svg" alt=""></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Возможность дистанционного обучения</div>
                                        <div class="kursi-info-item__text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        --><? /* endif; */ ?>
                    </div>
                </div>



                <? if ($arResult['PROPERTIES']['COUNT_DAY']['VALUE'] && $arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                    <div class="course__dates och">
                        <? foreach ($arResult['cities'] as $key => $city): ?>
                            <div class="city_<?= $key ?> date_range <?= $key === 0 ? 'active' : '' ?>">
                                <div class="course__block-title">Даты проведения курса в городе <?= $city ?></div>
                                <ul>
                                    <?
                                    foreach ($arResult['PROPERTIES']['DATE_START_NEW']['DESCRIPTION'] as $k => $c) {
                                        if ($c === $city) {
                                            $arDATE_start = ParseDateTime($arResult['PROPERTIES']['DATE_START_NEW']['VALUE'][$k], FORMAT_DATETIME);
                                            $arDATE_end = ParseDateTime($arResult['PROPERTIES']['DATE_END_NEW']['NEWVALUE'][$c][$k], FORMAT_DATETIME);
                                            ?>
                                            <li>
                                                <?= $arDATE_start["DD"] . " " . ToLower(GetMessage("MONTH_"
                                                    . (int)$arDATE_start["MM"] . "_S")) ?>
                                                - <?= $arDATE_end["DD"] . " " . ToLower(GetMessage("MONTH_"
                                                    . (int)$arDATE_end["MM"] . "_S")) ?>
                                            </li>
                                            <?
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
                <? if ($arResult['PROPERTIES']['DISTANCE']['VALUE'] === 'Да' || !$arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                    <div class="course__dates distant" style="<?= !$arResult['PROPERTIES']['HOURS']['VALUE'] ? 'display:block;' : '' ?>">
                        <div class="course__block-title">дистанционное обучение</div>
                        <p style="color: white;font-size: 20px;">
                            Обучение можно начать сразу же после оплаты
                        </p>
                        <br>
                        <p style="color: white;">
                            Обучение посредством онлайн платформы без участия преподавателя. Вы изучаете предоставленный материал в удобное вам время и по итогу
                            удаленно сдаете экзамен. Для обучения достаточно иметь устройство с доступом в Интернет. Документы о прохождении курса высылаем
                            почтой.
                        </p>
                    </div>
                <? endif; ?>
                <!--                --><? // if (!isset($_GET['city'])): ?>
                <!--                    <div class="course__dates">-->
                <!--                        <div class="course__block-title" style="font-size: 24px;">Даты проведения курса в вашем городе</div>-->
                <!--                        <p style="color: white">Чтобы узнать стоимость и даты курсов, выберете город.</p>-->
                <!--                    </div>-->
                <!--                --><? // endif; ?>
            </div>
        </div>

        <?

        $defimgs = [];
        if (!$arResult['DISPLAY_PROPERTIES']['PROP6']['FILE_VALUE'][0]) {
            $db_list = CIBlockSection::GetList(array(), $arFilter = array("IBLOCK_ID" => 4, "ID" => $arResult['IBLOCK_SECTION_ID']), true,
                $arSelect = array("UF_*"));
            while ($ar_result = $db_list->GetNext()) {
                $tmpimgs = $ar_result['UF_DEFIMGS'];
            }

            foreach ($tmpimgs as $tmpimg) {
                $defimgs[] = CFile::GetPath($tmpimg);
            }
        }

        ?>

        <? if ($arResult['DISPLAY_PROPERTIES']['PROP6']['FILE_VALUE'][0] || $defimg): ?>
            <div class="course__learning">
                <div class="course__subtitle">Учебный процесс</div>
                <div class="course__list _slickStudy">
                    <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP6']['FILE_VALUE'] as $img): ?>
                        <div class="course__item">
                            <img src="<?= $img['SRC'] ?>" alt="">
                        </div>
                    <? endforeach; ?>

                    <? if ($defimgs): ?>
                        <? foreach ($defimgs as $im): ?>
                            <div class="course__item">
                                <img src="<?= $im ?>" alt="">
                            </div>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
                <div class="navArrows _slickStudyArrow"></div>
            </div>
        <? endif; ?>
        <div class="course__consultation">
            <div class="consultation">
                <div class="consultation__content">
                    <div class="consultation__form">
                        <div class="consultation__title">Получите консультацию</div>
                        <div class="consultation__text">Расскажем Вам всё о наших услугах и ответим на все вопросы.</div>
                        <form action="" class="form" id="consult">
                            <div class="form__inputs">
                                <div class="form__input">
                                    <input type="text" class="input" placeholder="Ваше имя">
                                </div>
                                <div class="form__input">
                                    <input type="tel" class="input" placeholder="Телефон">
                                </div>
                            </div>
                            <div class="form__private">
                                <div class="checkbox">
                                    <input type="checkbox" id="private-2" checked>
                                    <label for="private-2">Согласен на обработку персональных данных </label>
                                </div>
                            </div>
                            <div class="form__btn">
                                <div class="btn-blue btn-md">Отправить заявку</div>
                            </div>

                        </form>
                    </div>
                    <div class="consultation__persona">
                        <div class="persona">
                            <div class="persona__info">
                                <div class="persona__name">Шаньшерова Марина Олеговна</div>
                                <div class="persona__position">Начальник отдела по работе с клиентами</div>
                            </div>
                            <div class="persona__img" style="background-image: url(/img/course/consultation-1.png)">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <? if ($arResult['DISPLAY_PROPERTIES']['PROP14']['VALUE']): ?>
            <div class="course__docs">
                <div class="kursi-docs-1">
                    <div class="kursi-docs-1__left">
                        <h2 class="section-title">Что вы получаете пройдя обучение в нашем центре</h2>
                        <p>По результатам обучения Вы получите:</p>
                        <ul class="markered-list">
                            <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP14']['DISPLAY_VALUE'] as $v => $val): ?>
                                <li><?= $val ?></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                    <div class="kursi-docs-1__right">
                        <ul class="docs-list">
                            <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP14']['LINK_ELEMENT_VALUE'] as $v => $val): ?>
                                <?
                                $f = CFile::GetPath($val["PREVIEW_PICTURE"]);
                                ?>
                                <li>
                                    <a href="<?= $f ?>" data-fancybox="gallery-docs2">
                                        <img src="<?= $f ?>" alt="">
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <? else: ?>
            <div class="course__docs">
                <div class="kursi-docs-1">
                    <div class="kursi-docs-1__left">
                        <h2 class="section-title">Что вы получаете пройдя обучение в нашем центре</h2>
                        <p>По результатам обучения Вы получите:</p>
                        <ul class="markered-list">
                            <li>Свидетельство о присвоении профессии</li>
                            <li>Диплом о профессиональной переподготовке</li>
                            <li>Удостоверение о повышении квалификации</li>
                            <li>Выписка из протокола заседания комиссии</li>
                        </ul>
                    </div>
                    <div class="kursi-docs-1__right">
                        <ul class="docs-list" style="max-height: 174px;">
                            <li>
                                <a href="/local/templates/cpreuro/assets/images/Свидетельство о присвоении профессии.jpg" data-fancybox="gallery-docs2">
                                    <img src="/local/templates/cpreuro/assets/images/Свидетельство о присвоении профессии.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="/local/templates/cpreuro/assets/images/Диплом о профессиональной переподготовке.png" data-fancybox="gallery-docs2">
                                    <img src="/local/templates/cpreuro/assets/images/Диплом о профессиональной переподготовке.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="/local/templates/cpreuro/assets/images/Приложение к диплому.png" data-fancybox="gallery-docs2">
                                    <img src="/local/templates/cpreuro/assets/images/Приложение к диплому.png" alt="">
                                </a>
                            </li>
                            <li style="display: flex;flex-direction: column;height: 175px;">
                                <a href="/local/templates/cpreuro/assets/images/Слой 1.png" data-fancybox="gallery-docs2">
                                    <img src="/local/templates/cpreuro/assets/images/Слой 1.png" alt="" style="height: 44px;">
                                </a>
                                <a href="/local/templates/cpreuro/assets/images/Выписка из протокола заседания комиссии.png" data-fancybox="gallery-docs2">
                                    <img src="/local/templates/cpreuro/assets/images/Выписка из протокола заседания комиссии.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <? if ($arResult['DISPLAY_PROPERTIES']['PROP7']['VALUE'][0]): ?>
            <div class="course__qualification">
                <div class="qualification">
                    <div class="course__subtitle">Получение разрядов</div>
                    <div class="qualification__list">
                        <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP7']['VALUE'] as $v => $val): ?>
                            <div class="qualification__item">
                                <div class="qualification__rate"><?= $val ?></div>
                                <div class="qualification__description"><?= $arResult['DISPLAY_PROPERTIES']['PROP7']['DESCRIPTION'][$v] ?></div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <? if ($arResult['DISPLAY_PROPERTIES']['PROP10']['VALUE']): ?>
            <div class="course__container course__container_v2">
                <div class="course__left">
                    <div class="course__subtitle"><?= $arResult['DISPLAY_PROPERTIES']['PROP10']['VALUE'] ?></div>
                    <div class="course__text _textAccordion">
                        <?= html_entity_decode($arResult['DISPLAY_PROPERTIES']['PROP8']['VALUE']['TEXT']) ?>
                    </div>
                    <div class="question_v2__more _moreInfo" data-text="Читать далее">Читать далее</div>
                </div>
                <div class="course__right">
                    <?
                    $textimg = '';
                    if (!$arResult['DISPLAY_PROPERTIES']['PROP6']['FILE_VALUE'][0]) {
                        $db_list = CIBlockSection::GetList(array(), $arFilter = array("IBLOCK_ID" => 4, "ID" => $arResult['IBLOCK_SECTION_ID']), true,
                            $arSelect = array("UF_*"));
                        while ($ar_result = $db_list->GetNext()) {
                            $textimg = CFile::GetPath($ar_result['UF_DEFTEXTIMG']);
                        }
                    }
                    ?>

                    <img src="<?= $arResult['DISPLAY_PROPERTIES']['PROP9']['FILE_VALUE']['SRC'] ?: $textimg ?>">
                </div>
            </div>
        <? endif; ?>

        <div class="course__advantages">
            <div class="course__subtitle">Преимущества центра</div>
            <div class="advantages">
                <div class="advantages__list _advantagesSlider">
                    <div class="advantages__item">
                        <div class="advantages__block">
                            <div class="advantages__img">
                                <img src="/img/course/prem-1.png" alt="">
                            </div>
                            <div class="advantages__title">профессионально обучаем с 1942 года</div>
                            <div class="advantages__text">Даем знания, которые работают. Гарантируем успешную аттестацию и выдачу удостоверения.</div>
                        </div>
                    </div>
                    <div class="advantages__item">
                        <div class="advantages__block">
                            <div class="advantages__img">
                                <img src="/img/course/prem-2.png" alt="">
                            </div>
                            <div class="advantages__title">Работаем по всей россии и за рубежом</div>
                            <div class="advantages__text">11 филиалов, собственные лаборатории и материально-техническая база.</div>
                        </div>
                    </div>
                    <div class="advantages__item">
                        <div class="advantages__block">
                            <div class="advantages__img">
                                <img src="/img/course/prem-3.png" alt="">
                            </div>
                            <div class="advantages__title">Разные форматы обучения</div>
                            <div class="advantages__text">Очное, дистанционное с индивидуальным графиком или выездное на предприятии для всего коллектива.</div>
                        </div>
                    </div>
                </div>
                <div class="navArrows _advantagesSliderArrow"></div>
            </div>
        </div>
        <div class="course__accreditation">
            <div class="accreditation">
                <div class="accreditation__content">
                    <div class="accreditation__info">
                        <div class="accreditation__title">Наши аккредитации</div>
                        <div class="accreditation__text">
                            АПО «НП Пермь-нефть» имеет необходимые лицензии и аккредитации для осуществления заявленных видов обучения. По окончании обучения
                            слушатели получают документы установленного образца
                        </div>
                        <a href="/dokumenty/" class="accreditation__link">Смотреть все аккредитации →</a>
                    </div>
                    <div class="accreditation__list _accreditationSlider">
                        <div class="accreditation__item">
                            <div class="accreditation__block">
                                <div class="accreditation__img">
                                    <a href="/local/templates/cpreuro/assets/doc/Лицензия на осуществление образовательной деятельности-01.jpg"
                                       data-fancybox="gallery-att">
                                        <img src="/local/templates/cpreuro/assets/doc/Лицензия на осуществление образовательной деятельности-01.jpg">
                                    </a>
                                </div>
                                <div class="accreditation__item-title">Лицензия на осуществление образовательной деятельности</div>
                            </div>
                        </div>
                        <div class="accreditation__item">
                            <div class="accreditation__block">
                                <div class="accreditation__img">
                                    <a href="/local/templates/cpreuro/assets/doc/Уведомление о внесении в реестр организаций, оказывающих услуги в области охраны труда-1.jpg"
                                       data-fancybox="gallery-att">
                                        <img src="/local/templates/cpreuro/assets/doc/Уведомление о внесении в реестр организаций, оказывающих услуги в области охраны труда-1.jpg">
                                    </a>
                                </div>
                                <div class="accreditation__item-title">Уведомление о внесении в реестр организаций, оказывающих услуги в области охраны труда
                                </div>
                            </div>
                        </div>
                        <div class="accreditation__item">
                            <div class="accreditation__block">
                                <div class="accreditation__img">
                                    <a href="/local/templates/cpreuro/assets/doc/Удостоверение об утверждении курсов ДОПОГ-1.jpg" data-fancybox="gallery-att">
                                        <img src="/local/templates/cpreuro/assets/doc/Удостоверение об утверждении курсов ДОПОГ-1.jpg">
                                    </a>
                                </div>
                                <div class="accreditation__item-title">Удостоверение об утверждении курсов ДОПОГ</div>
                            </div>
                        </div>
                        <div class="accreditation__item">
                            <div class="accreditation__block">
                                <div class="accreditation__img">
                                    <a href="/local/templates/cpreuro/assets/doc/Заключение Гостехнадзора-1.jpg" data-fancybox="gallery-att">
                                        <img src="/local/templates/cpreuro/assets/doc/Заключение Гостехнадзора-1.jpg">
                                    </a>
                                </div>
                                <div class="accreditation__item-title">Заключение Гостехнадзора</div>
                            </div>
                        </div>
                        <div class="accreditation__item">
                            <div class="accreditation__block">
                                <div class="accreditation__img">
                                    <a href="/local/templates/cpreuro/assets/doc/Сертификат IWCF-1.jpg" data-fancybox="gallery-att">
                                        <img src="/local/templates/cpreuro/assets/doc/Сертификат IWCF-1.jpg">
                                    </a>
                                </div>
                                <div class="accreditation__item-title">Сертификат IWCF</div>
                            </div>
                        </div>
                        <div class="accreditation__item">
                            <div class="accreditation__block">
                                <div class="accreditation__img">
                                    <a href="/local/templates/cpreuro/assets/doc/Сертификат соответствия СМК ИСО 9001-1.jpg" data-fancybox="gallery-att">
                                        <img src="/local/templates/cpreuro/assets/doc/Сертификат соответствия СМК ИСО 9001-1.jpg">
                                    </a>
                                </div>
                                <div class="accreditation__item-title">Сертификат соответствия СМК ИСО 9001</div>
                            </div>
                        </div>
                    </div>
                    <div class="navArrows _accreditationSliderArrow"></div>
                </div>
            </div>
        </div>


        <? if ($arResult['DISPLAY_PROPERTIES']['PROP11']['LINK_ELEMENT_VALUE']): ?>
            <div class="course__questions">
                <div class="course__subtitle">Вопрос-ответ</div>
                <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP11']['LINK_ELEMENT_VALUE'] as $el): ?>
                    <div class="question_v2 ">
                        <div class="question_v2__title"><?= $el['NAME'] ?></div>
                        <div class="question_v2__more _moreInfo" data-text="Посмотреть ответ">Посмотреть ответ</div>
                        <div class="question_v2__description">
                            <?
                            $res = CIBlockElement::GetByID($el['ID']);
                            if ($ar_res = $res->GetNext())
                                echo $ar_res['PREVIEW_TEXT']
                            ?>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endif; ?>
        <div class="course__consultation">
            <div class="consultation">
                <div class="consultation__content">
                    <div class="consultation__form">
                        <div class="consultation__title">У ВАС ОСТАЛИСЬ ВОПРОСЫ?</div>
                        <div class="consultation__text">Расскажем Вам всё о наших услугах и ответим на все вопросы.</div>
                        <form action="" class="form" id="questform">
                            <div class="form__inputs">
                                <div class="form__input">
                                    <input type="text" class="input" placeholder="Ваше имя">
                                </div>
                                <div class="form__input">
                                    <input type="tel" class="input" placeholder="Телефон">
                                </div>
                            </div>
                            <div class="form__private">
                                <div class="checkbox">
                                    <input type="checkbox" id="private" checked>
                                    <label for="private">Согласен на обработку персональных данных </label>
                                </div>
                            </div>
                            <div class="form__btn">
                                <div class="btn-blue btn-md">Отправить заявку</div>
                            </div>

                        </form>
                    </div>
                    <div class="consultation__persona">
                        <div class="persona persona_v2">
                            <div class="persona__info">
                                <div class="persona__name">Кощеева Анастасия Алексеевна</div>
                                <div class="persona__position">Специалист центра профессионального обучения</div>
                            </div>
                            <div class="persona__img" style="background-image: url(/img/course/consultation-2.png)">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="course__gratitude">
            <div class="course__subtitle">Благодарности</div>
            <div class="gratitude">
                <a href="/sveden/blagodarstvennye-pisma/" class="gratitude__more">Смотреть все →</a>
                <div class="gratitude__list _slickGratitude">
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b1.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b1.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b2.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b2.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b3.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b3.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b4.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b4.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b5.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b5.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b6.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b6.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b7.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b7.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b8.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b8.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b9.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b9.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b10.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b10.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b11.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b11.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gratitude__item">
                        <div class="gratitude__block">
                            <div class="gratitude__img">
                                <a href="/local/templates/cpreuro/assets/images/b12.jpg" data-fancybox="gallery-reviews">
                                    <img src="/local/templates/cpreuro/assets/images/b12.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navArrows _slickGratitudeArrows"></div>
            </div>
        </div>

        <? if ($arResult['DISPLAY_PROPERTIES']['PROP12']['LINK_ELEMENT_VALUE']): ?>
            <div class="course__reviews">
                <div class="course__subtitle">Отзывы</div>
                <div class="reviews">
                    <div class="reviews__list _sliderReviews">
                        <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP12']['LINK_ELEMENT_VALUE'] as $val): ?>
                            <div class="reviews__item">
                                <div class="reviews__block">
                                    <div class="reviews__title"><?= $val['NAME'] ?></div>
                                    <div class="reviews__text">
                                        <?
                                        $res = CIBlockElement::GetByID($val['ID']);
                                        if ($ar_res = $res->GetNext())
                                            echo $ar_res['PREVIEW_TEXT']
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="navArrows _sliderReviewsArrows"></div>
                </div>
            </div>
        <? endif; ?>


        <div class="course__program">
            <div class="moreProgram">
                <div class="moreProgram__content">
                    <div class="moreProgram__bg" style="background-image: url(/img/course/programs.png)"></div>
                    <div class="moreProgram__block">
                        <div class="moreProgram__title">еЩЁ 642 учебные специальности</div>
                        <div class="moreProgram__text">Подготовка специалистов-представителей межотраслевых профессий,
                            предлагаем специализированные программы переподготовки руководителей. Курсы профессиональной
                            переподготовки, повышения квалификации.
                        </div>
                        <div class="moreProgram__btn"><a class="btn-md btn-white" href="/kursy/">Подобрать курс</a></div>
                    </div>
                </div>
            </div>
        </div>

        <? if ($arResult['DISPLAY_PROPERTIES']['PROP13']['LINK_ELEMENT_VALUE']): ?>
        <div class="course__other">
            <div class="course__subtitle">Вместе с этим курсом выбирают</div>
            <div class="otherCourse">
                <div class="otherCourse__content">
                    <? $c = 0; ?>
                    <? foreach ($arResult['DISPLAY_PROPERTIES']['PROP13']['LINK_ELEMENT_VALUE'] as $i => $item): ?>
                        <? if ($c % 3 === 0): ?>
                            <div class="otherCourse__col">
                        <? endif; ?>
                        <div class="otherCourse__item">
                            <div class="otherCourse__title"><?= $item['NAME'] ?></div>
                            <div class="otherCourse__text">По заявкам <br>
                                <?
                                $res = CIBlockElement::GetByID($item['ID']);
                                if ($ar_res = $res->GetNext())
                                    echo $ar_res['PREVIEW_TEXT']
                                ?>
                            </div>
                            <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="otherCourse__more">Подробнее →</a>
                        </div>
                        <? if ($c % 3 === 2): ?>
                            </div>
                        <? endif; ?>
                        <? $c++; ?>
                    <? endforeach; ?>
                    <? if ($c !== 2): ?>
                </div>
                <? endif; ?>
            </div>
        </div>
    </div>
    <? else: ?>
        <div class="course__other">
            <div class="course__subtitle">Вместе с этим курсом выбирают</div>
            <div class="otherCourse">
                <div class="otherCourse__content">
                    <?
                    $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_PAGE_URL");
                    $arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_SECTION_ID" => $arResult['IBLOCK_SECTION_ID']);
                    $res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array(), $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arRands[] = $ob->GetFields();
                    }
                    ?>

                        <? $c = 0; ?>
                        <? foreach ($arRands as $i => $item): ?>
                            <?
                                if ($i >=5) {
                                    break;
                                }
                            ?>
                            <? if ($c % 3 === 0): ?>
                                <div class="otherCourse__col">
                            <? endif; ?>
                            <div class="otherCourse__item">
                                <div class="otherCourse__title"><?= $item['NAME'] ?></div>
                                <div class="otherCourse__text">По заявкам <br>
                                    <?
                                        echo $item['PREVIEW_TEXT']
                                    ?>
                                </div>
                                <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="otherCourse__more">Подробнее →</a>
                            </div>
                            <? if ($c % 3 === 2): ?>
                                </div>
                            <? endif; ?>
                            <? $c++; ?>
                        <? endforeach; ?>
                        <? if ($c !== 2): ?>
                    </div>
                <? endif; ?>


                </div>
            </div>
        </div>
    <? endif; ?>
</div>
</div>


<div style="display: none">
    <div class="kursi-first">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="kursi-first__left">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:breadcrumb",
                            "bread",
                            array(
                                "PATH" => "",
                                "SITE_ID" => "s1",
                                "START_FROM" => "0"
                            )
                        ); ?>
                        <h1 class="kursi-first__title"><?= $arResult['NAME'] ?></h1>
                        <div class="kursi-first__text">
                            <?= $arResult["DETAIL_TEXT"] ?>
                        </div>
                        <div class="kursi-first__btn">
                            <button class="btn-blue btn-md" data-toggle="modal" data-target="#orderModal">
                                Подать заявку
                            </button>
                            <? if ($arResult['PROPERTIES']['DISTANCE']['VALUE'] === 'Да'): ?>
                                <button class="btn-blue btn-md dist_btn" data-toggle="modal" data-target="#orderModal2">
                                    Дистанционное обучение
                                </button>
                                <style>
                                    .dist_btn {
                                        background: none;
                                        border: 1px solid #0065c8;
                                        color: #0065c8;
                                        transition: 0.3s;
                                    }

                                    .dist_btn:hover {
                                        background: linear-gradient(56.23deg, #0065C8 10.19%, #0090F2 85.13%);
                                        color: #FFFFFF;
                                    }
                                </style>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="kursi-first__right">
                        <div class="kursi-info-list">
                            <? if ($arResult['PROPERTIES']['HOURS']['VALUE']): ?>
                                <div class="kursi-info-list__item">
                                    <div class="kursi-info-item">
                                        <div class="kursi-info-item__ic"><img
                                                    src="/local/templates/cpreuro/assets/images/info-ic-2.svg"></div>
                                        <div class="kursi-info-item__main">
                                            <div class="kursi-info-item__title">Количество<br> часов</div>
                                            <div class="kursi-info-item__text"><?= $arResult['PROPERTIES']['HOURS']['VALUE'] ?>
                                                ч
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                            <? if ($arResult['PROPERTIES']['PRICE']['VALUE']): ?>
                                <div class="kursi-info-list__item">
                                    <div class="kursi-info-item">
                                        <div class="kursi-info-item__ic"><img
                                                    src="/local/templates/cpreuro/assets/images/price.svg"></div>
                                        <div class="kursi-info-item__main">
                                            <div class="kursi-info-item__title">Стоимость обучения</div>
                                            <div class="kursi-info-item__text">
                                                <? if ($_GET['city']): ?>
                                                    <?
                                                    $key = array_search($_GET['city'], $arResult['PROPERTIES']['PRICE_NEW']['DESCRIPTION'], true);
                                                    $p = $arResult['PROPERTIES']['PRICE_NEW']['VALUE'][$key];
                                                    if ($p):?>
                                                        от <?= $p ?> руб
                                                    <? else: ?>
                                                        от <?= $arResult['PROPERTIES']['PRICE']['VALUE'] ?> руб
                                                    <? endif; ?>
                                                <? else: ?>
                                                    от <?= $arResult['PROPERTIES']['PRICE']['VALUE'] ?> руб
                                                <? endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                            <? if ($arResult['PROPERTIES']['DOC']['VALUE']): ?>
                                <div class="kursi-info-list__item">
                                    <div class="kursi-info-item">
                                        <div class="kursi-info-item__ic"><img
                                                    src="/local/templates/cpreuro/assets/images/doc.svg"></div>
                                        <div class="kursi-info-item__main">
                                            <div class="kursi-info-item__title">Документ</div>
                                            <div class="kursi-info-item__text"><?= $arResult['PROPERTIES']['DOC']['VALUE'] ?></div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/info-ic-4.svg"></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Сроки<br> обучения</div>
                                        <div class="kursi-info-item__text">
                                            <? if ($_GET['city']): ?>
                                                <?
                                                if (!$arResult['PROPERTIES']['COUNT_DAY']['VALUE']) {
                                                    ?>по заявкам<?
                                                }
                                                else {
                                                    $count_date = 0;
                                                    foreach ($arResult['PROPERTIES']['DATE_START_NEW']['DESCRIPTION'] as $k => $c) {
                                                        if ($c === $_GET['city']) {
                                                            $count_date++;
                                                            if ($count_date > 3) {
                                                                break;
                                                            }
                                                            $arDATE_start = ParseDateTime($arResult['PROPERTIES']['DATE_START_NEW']['VALUE'][$k], FORMAT_DATETIME);
                                                            $arDATE_end = ParseDateTime($arResult['PROPERTIES']['DATE_END_NEW']['VALUE'][$k], FORMAT_DATETIME);
                                                            ?>
                                                            <div class="time_range">
                                                                <?= $arDATE_start["DD"] . " " . ToLower(GetMessage("MONTH_"
                                                                    . (int)$arDATE_start["MM"] . "_S")) ?>
                                                                - <?= $arDATE_end["DD"] . " " . ToLower(GetMessage("MONTH_"
                                                                    . (int)$arDATE_end["MM"] . "_S")) ?>
                                                            </div>
                                                            <?
                                                        }
                                                    }
                                                }
                                                ?>
                                                <? if ($count_date > 3): ?>
                                                    <a href="#all_date">Смотреть все даты</a>
                                                <? elseif ($count_date === 0): ?>
                                                    по заявкам
                                                <? endif; ?>
                                            <? else: ?>
                                                Что бы узнать даты обучения<br>
                                                <a data-toggle="modal" data-target="#callModal_city"
                                                   style="cursor: pointer">выберите
                                                    город</a>
                                            <? endif; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kursi-info-list__item">
                                <div class="kursi-info-item">
                                    <div class="kursi-info-item__ic"><img
                                                src="/local/templates/cpreuro/assets/images/info-ic-5.svg" alt=""></div>
                                    <div class="kursi-info-item__main">
                                        <div class="kursi-info-item__title">Место обучения</div>
                                        <div class="kursi-info-item__text">
                                            <? if ($_GET['city']): ?>
                                                <?
                                                $arPlace = [
                                                    'Пермь' => 'г. Пермь, бульвар Гагарина, 54а',
                                                    'Березники' => 'г. Березники, проезд Сарычева,1',
                                                    'Чернушка' => 'г. Чернушка, ул. Коммунистическая, 11',
                                                    'Киров' => 'г. Киров, ул. Чапаева, 5, корп. 2',
                                                    'Краснокамск' => 'г. Краснокамск, ул. Геофизиков, 14',
                                                    'Кунгур' => 'г. Кунгур, ул. Нефтяников, 2',
                                                    'Оха' => 'г. Оха, ул. Советская, 26',
                                                    'Оса' => 'г. Оса, ул. Степана Разина, 79',
                                                    'Полазна' => 'пгт. Полазна, ул. Парковая, 12',
                                                    'Усинск' => 'г. Усинск, ул. Нефтяников, 21',
                                                ];
                                                $p = $arPlace[$_GET['city']];
                                                ?>
                                                <?= $p ?>
                                                <br>
                                                <a data-toggle="modal" data-target="#callModal_city"
                                                   style="cursor: pointer">Выбрать другой город</a>
                                            <? else: ?>
                                                Что бы узнать место где проводится обучение<br>
                                                <a data-toggle="modal" data-target="#callModal_city"
                                                   style="cursor: pointer">выберите
                                                    город</a>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <? if ($arResult['PROPERTIES']['DISTANCE']['VALUE'] === 'Да'): ?>
                                <div class="kursi-info-list__item">
                                    <div class="kursi-info-item">
                                        <div class="kursi-info-item__ic"><img
                                                    src="/local/templates/cpreuro/assets/images/dis.svg" alt=""></div>
                                        <div class="kursi-info-item__main">
                                            <div class="kursi-info-item__title">Возможность дистанционного обучения</div>
                                            <div class="kursi-info-item__text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kursi-second" style="margin-top: 10px;">
        <? if ($arResult['PROPERTIES']['DISTANCE']['VALUE'] === 'Да'): ?>
            <div class="container">
                <div class="kursi-second-content" style="display: flex;margin-bottom: 86px;">
                    <div class="kursi-capture" style="background: linear-gradient(0deg, rgba(0, 101, 200, 0.82), rgba(0, 101,
        200, 0.82)), url(/local/templates/cpreuro/assets/images/f.png) center top
        no-repeat;padding: 57px 35px;">
                        <div class="section-title">дистанционное обучене</div>
                        <div class="kursi-capture__text" style="width: 777px;max-width: unset">
                            При наличии среднего профессионального или высшего образования
                            Вы можете пройти курс проф переподготовки в дистанционном формате
                        </div>
                    </div>
                    <div class="kursi-capture_info"
                         style="margin-top: 80px;padding: 20px 34px; background-color: #F4F4F4;width:100%;">
                        <div class="capture_info__wrapper"
                             style="font-size: 40px;line-height: 50px;font-family: Fedra SansPro;font-weight: 600;">
                            <div class="capture_info__h">
                                <div class="wrapper" style="display: flex;flex-wrap: wrap;">
                                    <span style="color: #0065c8;"><?= $arResult['PROPERTIES']['HOURS_DIS']['VALUE'] ?></span>&nbsp;ч
                                    <span class="capture_info__count" style="color: #838383;font-size: 18px;line-height: 22px;font-weight: 500;">Количество часов</span>
                                </div>
                            </div>
                            <div class="capture_info__p">
                                <div class="wrapper" style="display: flex;flex-wrap: wrap;">
                                    <span style="color: #0065c8;"><?= $arResult['PROPERTIES']['PRICE_DIS']['VALUE'] ?></span>&nbsp;руб
                                    <span class="capture_info__count" style="color: #838383;font-size: 18px;line-height: 22px;font-weight: 500;">Цена
                                курса</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <? if ($_GET['city'] && $count_date > 3 && $arResult['PROPERTIES']['COUNT_DAY']['VALUE']): ?>
            <div class="container">
                <div class="sroki" style="margin-bottom: 86px;">
                    <a name="all_date"></a>
                    <div class="section-title">Сроки обучения на 2020-21 год</div>
                    <div class="section-title_text">Даты проведения курса в городе <?= $_GET['city'] ?></div>
                    <div class="sroki__wrapper" style="display: flex;margin-top: 19px;">
                        <?
                        foreach ($arResult['PROPERTIES']['DATE_START_NEW']['DESCRIPTION'] as $k => $c) {
                            if ($c === $_GET['city']) {
                                $arDATE_start = ParseDateTime($arResult['PROPERTIES']['DATE_START_NEW']['VALUE'][$k], FORMAT_DATETIME);
                                $arDATE_end = ParseDateTime($arResult['PROPERTIES']['DATE_END_NEW']['VALUE'][$k], FORMAT_DATETIME);
                                ?>
                                <div class="srok_item" style="margin-right: 69px;">
                                    <?= $arDATE_start["DD"] . " " . ToLower(GetMessage("MONTH_"
                                        . (int)$arDATE_start["MM"] . "_S")) ?>
                                    - <?= $arDATE_end["DD"] . " " . ToLower(GetMessage("MONTH_"
                                        . (int)$arDATE_end["MM"] . "_S")) ?>
                                </div>
                                <?
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <div class="container kursi-about-list-content">
            <h2 class="section-title">Учебный центр<br> «Пермь-Нефть» это:</h2>
            <div class="kursi-about-list">
                <div class="kursi-about-list__item">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/kursy/1_block.php"
                        )
                    ); ?>
                </div>
                <div class="kursi-about-list__item">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/kursy/2_block.php"
                        )
                    ); ?>
                </div>
                <div class="kursi-about-list__item">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/kursy/3_block.php"
                        )
                    ); ?>
                </div>
                <div class="kursi-about-list__item">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/kursy/4_block.php"
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
    <? if ($arResult['PROPERTIES']['BANNER']['VALUE']): ?>
        <?
        $arSelect = array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "PREVIEW_TEXT", 'PREVIEW_PICTURE');
        $arFilter = array("IBLOCK_ID" => 9, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $arResult['PROPERTIES']['BANNER']['VALUE']);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $banner = [
                'name' => $arFields['NAME'],
                'text_btn' => $arProps['TEXT']['VALUE'],
                'link_btn' => $arProps['LINK']['VALUE'],
                'color_btn' => $arProps['COLOR_BTN']['VALUE'],
                'text' => $arFields['PREVIEW_TEXT'],
                'img' => CFile::GetFileArray($arFields['PREVIEW_PICTURE'])["SRC"],
            ];
        }
        ?>
        <div class="container">
            <div class="kursi-capture" style="background: url(<?= $banner['img'] ?>) center top no-repeat;">
                <div class="section-title"><?= $banner['name'] ?></div>
                <div class="kursi-capture__text">
                    <?= $banner['text'] ?>
                </div>
                <div class="kursi-capture__btn"><a class="btn-white btn-md" href="<?= $banner['link_btn'] ?>"
                                                   style="color: <?= $banner['color_btn'] ?>;"><?= $banner['text_btn'] ?></a>
                </div>
            </div>
        </div>
    <? endif; ?>
    <? if ($arResult['PROPERTIES']['IMG_FOR_BIG_TEXT']['VALUE']): ?>
        <div class="container">
            <div class="kursi-specialist">
                <div class="kursi-specialist__left">
                    <img src="<?= CFile::GetFileArray($arResult['PROPERTIES']['IMG_FOR_BIG_TEXT']['VALUE'])["SRC"] ?>">
                </div>
                <div class="kursi-specialist__right">
                    <?= html_entity_decode($arResult['PROPERTIES']['BIG_TEXT']['VALUE']['TEXT']) ?>
                </div>
            </div>
        </div>
    <? endif; ?>
    <div class="container">
        <div class="kursi-capture-2">
            <div class="kursi-capture-2__left">
                <div>
                    <div class="section-title">Стоимость курса</div>
                    <div class="kursi-capture-2__left-text">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/kursy/form_text.php"
                            )
                        ); ?>
                    </div>
                </div>
                <div class="kursi-capture-2__left-btm">
                    <div class="kursi-capture-2__left-btm-text">Стоимость курса <?= $arResult['NAME'] ?></div>
                    <div class="kursi-capture-2__left-btm-price">от
                        <span> <?= $arResult['PROPERTIES']['PRICE']['VALUE'] ?></span> рублей.
                    </div>
                </div>
            </div>
            <div class="kursi-capture-2__right">
                <form id="course_moreinfo_form" type="post">
                    <div class="kursi-capture-form">
                        <h2 class="section-title">Уточнить стоимость курса</h2>
                        <input type="hidden" name="course" value="<?= $arResult['NAME'] ?>">
                        <input type="hidden" name="action" value="callback">
                        <div class="kursi-capture-form__textfield">
                            <input class="n-textfield" type="text" name="name" placeholder="Ваше имя *" required>
                        </div>
                        <div class="kursi-capture-form__textfield">
                            <input class="n-textfield" type="tel" name="phone" placeholder="Телефон *"

                                   title="Международный, государственный или местный телефонный номер" required>
                            <!-- pattern="(\+?\d[- .]*){7,13}"--></div>
                        <div class="kursi-capture-form__checkbox">
                            <label class="n-checkbox" for="checkbox-1">
                                <input type="checkbox" id="checkbox-1" class="styler-disabled">
                                <span> </span>
                            </label>
                            <label class="n-ckeckbox-label" for="checkbox-1">Согласен на обработку персональных
                                данных</label>
                        </div>
                        <div class="kursi-capture-form__btn">
                            <button class="btn-md btn-blue">Отправить</button>
                        </div>
                    </div>
                    <input type="hidden" name="form" value="course_moreinfo_form">
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="kursi-docs-1">
            <div class="kursi-docs-1__left">
                <h2 class="section-title">Что вы получаете пройдя обучение в нашем центре</h2>
                <p>По результатам обучения Вы получите:</p>
                <ul class="markered-list">
                    <li>удостоверение о присвоении профессии</li>
                    <li>свидетельство</li>
                    <li>выписку из протокола заседания комиссии</li>
                </ul>
            </div>
            <div class="kursi-docs-1__right">
                <ul class="docs-list">
                    <li><a><img src="/local/templates/cpreuro/assets/images/doc-1.jpg"></a></li>
                    <li><a><img src="/local/templates/cpreuro/assets/images/doc-2.jpg"></a></li>
                    <li><a><img src="/local/templates/cpreuro/assets/images/doc-3.jpg"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="kursi-docs-2">
            <div class="kursi-docs-2__left">
                <ul class="docs-list docs-list_2">
                    <li>
                        <a href="/local/templates/cpreuro/assets/images/ak1.jpg" data-fancybox="gallery-docs">
                            <img src="/local/templates/cpreuro/assets/images/ak1.jpg">
                        </a>
                    </li>
                    <li>
                        <a href="/local/templates/cpreuro/assets/images/ak2.jpg" data-fancybox="gallery-docs">
                            <img src="/local/templates/cpreuro/assets/images/ak2.jpg">
                        </a>
                    </li>
                    <li>
                        <a href="/local/templates/cpreuro/assets/images/ak3.jpg" data-fancybox="gallery-docs">
                            <img src="/local/templates/cpreuro/assets/images/ak3.jpg">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="kursi-docs-2__right">
                <h2 class="section-title">Наши аккредитации</h2>
                <p>
                    С нашей помощью Вы получить качественно выполненную услугу в Перми. Наш центр входит в Единый
                    Реестр органов по сертификации и испытательных лабораторий. Является аккредитованным
                    представителем
                </p>
                <a class="n-link kursi-docs-2__link" href="/dokumenty/">Смотреть все аккредитации</a>
            </div>
        </div>
    </div>
    <div class="container">
        <form id="course_questions_form" action="javascript:alert('Form error!');" type="post">
            <div class="kursi-capture-3">
                <div class="kursi-capture-3__img"><img src="/local/templates/cpreuro/assets/images/kursi-capture-3-img.png">
                </div>
                <div class="section-title">У Вас остались вопросы?</div>
                <div class="kursi-capture-3__subtitle">Расскажем Вам всё о наших услугах и ответим на все вопросы.</div>
                <div class="kursi-capture-3__row">
                    <input type="hidden" name="course" value="<?= $arResult['NAME'] ?>">
                    <input type="hidden" name="action" value="callback">
                    <div class="kursi-capture-3__textfield">
                        <input class="n-textfield" type="text" name="name" placeholder="Ваше имя *" required="">
                    </div>
                    <div class="kursi-capture-3__textfield">
                        <input class="n-textfield" type="text" name="phone" placeholder="Телефон *"
                               pattern="(\+?\d[- .]*){7,13}"
                               title="Международный, государственный или местный телефонный номер" required="">
                    </div>
                    <div class="kursi-capture-3__btn">
                        <button class="btn-md btn-blue">Отправить</button>
                    </div>
                </div>
                <div class="kursi-capture-3__checkbox">
                    <label class="n-checkbox" for="checkbox-2">
                        <input type="checkbox" id="checkbox-2" class="styler-disabled">
                        <span> </span>
                    </label>
                    <label class="n-ckeckbox-label" for="checkbox-2">Согласен на обработку персональных данных</label>
                </div>
            </div>
            <input type="hidden" name="form" value="course_questions_form">
        </form>
    </div>
    <div class="container">
        <div class="kursi-reviews">
            <div class="section-title">Благодарности</div>
            <div class="kursi-reviews__slider">
                <div class="kursi-reviews-slider js-reviews-slider">
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b1.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b1.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b2.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b2.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b3.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b3.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b4.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b4.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b5.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b5.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b6.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b6.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b7.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b7.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b8.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b8.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b9.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b9.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b10.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b10.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b11.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b11.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b12.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b12.jpg" alt="">
                        </a>
                    </div>
                    <div class="kursi-reviews-slider__slide">
                        <a href="/local/templates/cpreuro/assets/images/b13.jpg" data-fancybox="gallery-reviews">
                            <img src="/local/templates/cpreuro/assets/images/b13.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="kursi-reviews__link">
                <a class="n-link" href="/sveden/blagodarstvennye-pisma/">Смотреть все благодарности</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="kursi-advantages">
            <h2 class="section-title">Преимущества центра</h2>
            <div class="kursi-advantages-list">
                <div class="kursi-advantages-list__item">
                    <div class="kursi-advantage-item">
                        <div class="kursi-advantage-item__ic"><img
                                    src="/local/templates/cpreuro/assets/images/advantage-ic-1.svg"></div>
                        <div class="kursi-advantage-item__text">
                            77 лет успешной работы на рынке профессионального
                            образования
                        </div>
                    </div>
                </div>
                <div class="kursi-advantages-list__item">
                    <div class="kursi-advantage-item">
                        <div class="kursi-advantage-item__ic"><img
                                    src="/local/templates/cpreuro/assets/images/advantage-ic-2.svg"></div>
                        <div class="kursi-advantage-item__text">Более 500 образовательных курсов</div>
                    </div>
                </div>
                <div class="kursi-advantages-list__item">
                    <div class="kursi-advantage-item">
                        <div class="kursi-advantage-item__ic"><img
                                    src="/local/templates/cpreuro/assets/images/advantage-ic-3.svg"></div>
                        <div class="kursi-advantage-item__text">
                            Удобные формы обучения с применением дистанционных
                            технологий
                        </div>
                    </div>
                </div>
                <div class="kursi-advantages-list__item">
                    <div class="kursi-advantage-item">
                        <div class="kursi-advantage-item__ic"><img
                                    src="/local/templates/cpreuro/assets/images/advantage-ic-4.svg"></div>
                        <div class="kursi-advantage-item__text">Высокий уровень подготовки слушателей</div>
                    </div>
                </div>
                <div class="kursi-advantages-list__item">
                    <div class="kursi-advantage-item">
                        <div class="kursi-advantage-item__ic"><img
                                    src="/local/templates/cpreuro/assets/images/advantage-ic-5.svg"></div>
                        <div class="kursi-advantage-item__text">Квалифицированный состав опытных преподавателей</div>
                    </div>
                </div>
                <div class="kursi-advantages-list__item">
                    <div class="kursi-advantage-item">
                        <div class="kursi-advantage-item__ic"><img
                                    src="/local/templates/cpreuro/assets/images/advantage-ic-6.svg"></div>
                        <div class="kursi-advantage-item__text">Работаем со всей Россией и зарубежными странами</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="kursi-capture-4">
            <div class="kursi-capture-4__left">
                <div class="kursi-capture-4__num-text">более</div>
                <div class="kursi-capture-4__num">500</div>
            </div>
            <div class="kursi-capture-4__right">
                <div class="section-title">учебных специальностей и программ</div>
                <div class="kursi-capture-4__text">
                    Подготовка специалистов-представителей межотраслевых профессий,
                    предлагаем специализированные программы переподготовки руководителей.
                    Курсы профессиональной переподготовки, повышения квалификации.
                </div>
                <div class="kursi-capture-4__btn"><a class="btn-md btn-white" href="/kursy/">Подобрать курс</a></div>
            </div>
        </div>
    </div>
</div>


<?
foreach ($arResult['PROPERTIES']['CITY']['VALUE'] as $city) {
    $res = CIBlockElement::GetByID($city);
    if ($ar_res = $res->GetNext())
        $arCites[] = [
            'NAME' => $ar_res['NAME'],
            'ID' => $ar_res['ID']
        ];
}
?>
<div class="modal fade modal_custom" id="callModal_city" tabindex="-1" role="dialog"
     aria-labelledby="callModal_cityLabel" aria-hidden="true"
     style="display: none;top: 50%;transform: translateY(-50%);">
    <div class="modal-dialog" style="width: 320px;">
        <div class="modal-content">
            <div class="modal-body">
                <div class="top text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2>Выбрать<br>город</h2>
                </div>
                <div class="city_wrapper" style="display: flex;flex-flow: column wrap;max-height: 100px;padding: 0
                28px;">
                    <? foreach ($arCites as $cite): ?>
                        <div class="city_item" style="color: #4F4F4F;">
                            <a href="?city=<?= $cite['NAME'] ?>"><?= $cite['NAME'] ?></a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>