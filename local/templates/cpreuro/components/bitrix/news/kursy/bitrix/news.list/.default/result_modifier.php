<?php
$arResult['NAV_NUM'] = $arResult['NAV_RESULT']->NavNum;
$arResult['NAV_PAGE_NOMER'] = $arResult['NAV_RESULT']->NavPageNomer;
$arResult['NAV_PAGE_COUNT'] = $arResult['NAV_RESULT']->NavPageCount;
$arResult['SECTION_CODE'] = $arParams["SECTION_CODE"];
$this->__component->SetResultCacheKeys([
    'NAV_NUM',
    'NAV_PAGE_NOMER',
    'NAV_PAGE_COUNT',
    'SECTION_CODE',
]);

$arSelect = Array(
    "ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", 'CODE', 'XML_ID', 'ACTIVE', 'DATE_ACTIVE_FROM',
    'DATE_ACTIVE_TO', 'SORT', 'PREVIEW_TEXT', 'PREVIEW_TEXT_TYPE', 'DETAIL_TEXT', 'DETAIL_TEXT_TYPE', 'DATE_CREATE',
    'CREATED_BY', 'TAGS', 'TIMESTAMP_X', 'MODIFIED_BY', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL', 'DETAIL_PICTURE',
    'PREVIEW_PICTURE', 'LANG_DIR', 'SHOW_COUNTER', 'SHOW_COUNTER_START_X', 'EXTERNAL_ID', 'IBLOCK_TYPE_ID',
    'IBLOCK_CODE', 'IBLOCK_EXTERNAL_ID', 'LID', 'ACTIVE_FROM', 'ACTIVE_TO', 'CATALOG_TYPE', "PROPERTY_*",
    'DISPLAY_PROPERTIES'
);
$arFilter = Array("IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_ADD" => $arResult["SECTION"]["PATH"][0]['ID'], "NAME" => 'ASC');
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();

    $el = [
        'PRICES' => [],
        'PRICE_MATRIX' => false,
        'MIN_PRICE' => $arProps['ID'],
        'DISPLAY_PROPERTIES' => [],
        'PRODUCT_PROPERTIES' => [],
        'PRODUCT_PROPERTIES_FILL' => [],
    ];

    $prop['PROPERTIES'] = $arProps;

    $elRes = array_merge($el, $arFields, $prop);
    $arResult['ITEMS'][] = $elRes;
}