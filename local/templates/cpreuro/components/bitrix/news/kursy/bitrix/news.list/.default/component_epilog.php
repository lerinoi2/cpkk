<?php

if (
    isset($arResult['NAV_RESULT_NAV_NUM'], $arResult['NAV_RESULT_NAV_PAGE_NOMER'], $arResult['SECTION_SECTION_PAGE_URL'])
    && (
        array_key_exists('PAGEN_' . $arResult['NAV_RESULT_NAV_NUM'], $_GET)
        || $arResult['NAV_RESULT_NAV_PAGE_NOMER'] > 1
    )
) {
    $APPLICATION->SetPageProperty('canonical', 'https://' . $_SERVER['HTTP_HOST'] . $APPLICATION->GetCurPage(false));
}

$arResult['URL'] = $APPLICATION->GetCurPage(false);
if ($arResult['NAV_PAGE_NOMER'] > 1) {
    $APPLICATION->SetPageProperty('canonical', 'https://' . $_SERVER['HTTP_HOST'] . $APPLICATION->GetCurPage(false));
}

if (isset($arResult['NAV_NUM'], $arResult['NAV_PAGE_NOMER'], $arResult['NAV_PAGE_COUNT'], $arResult['URL'])) {
    if ($arResult['NAV_PAGE_COUNT'] > $arResult['NAV_PAGE_NOMER']) { // rel next
        $next = $arResult['NAV_PAGE_NOMER'] + 1;
        $urlNextRel = $arResult['URL'] . "?PAGEN_1=" . $next;
    }
    if ($arResult['NAV_PAGE_NOMER'] > 1) { // rel prev
        $prev = $arResult['NAV_PAGE_NOMER'] - 1;
        If ($prev > 1) {
            $urlPrevRel = $arResult['URL'] . "?PAGEN_1=" . $prev;
        }
        else {
            $urlPrevRel = $arResult['URL'];
        }
    }
    if (isset($urlNextRel)) {
        $APPLICATION->SetPageProperty('next', 'https://' . $_SERVER["HTTP_HOST"] . $urlNextRel);
    }
    if (isset($urlPrevRel)) {
        $APPLICATION->SetPageProperty('prev', 'https://' . $_SERVER["HTTP_HOST"] . $urlPrevRel);
    }
}