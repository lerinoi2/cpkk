<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $arrFilter;
if ($arrFilter['PROPERTY']['?DATE_START']) {
    $arrFilter['PROPERTY']['?DATE_START'] = trim(CDatabase::CharToDateFunction(ConvertTimeStamp(strtotime($arrFilter['PROPERTY']['?DATE_START'])), 'SHORT'), "\'");
}
if ($arrFilter['PROPERTY']['?DATE_END']) {
    $arrFilter['PROPERTY']['?DATE_END'] = trim(CDatabase::CharToDateFunction(ConvertTimeStamp(strtotime($arrFilter['PROPERTY']['?DATE_END'])), 'SHORT'), "\'");
}
?>

<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get"
      class="courses_search">
    <div class="row">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                <?
                switch ($arItem['NAME']) {
                    case 'Название':
                        ?>
                        <div class="col-xs-6 col-xxs-12">
                            <div class="form-group">
                                <input type="text" name="arrFilter_ff[NAME]" size="20" value="" class="form-control"
                                       placeholder="Название курса" list="courses">
                                <datalist id="courses">
                                    <option></option>
                                    <? foreach ($arResult['ITEMS_F'] as $item): ?>
                                        <option><?= $item['NAME'] ?></option>
                                    <? endforeach; ?>
                                </datalist>
                            </div>
                        </div>
                        <?
                        break;
                    case 'Город':
                        ?>
                        <div class="col-xs-6 col-xxs-12">
                            <div class="form-group">
                                <select name="arrFilter_pf[CITY]" id="">
                                    <option value="">Выберите город</option>
                                    <? foreach ($arResult['ITEMS_C'] as $city): ?>
                                        <option value="<?= $city['ID'] ?>"><?= $city['NAME'] ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <?
                        break;
                    default:
                        ?>
                        <div class="col-xs-6 col-xxs-12">
                            <div class="form-group">
                                <input type="text" class="form-control datepicker" name="<?= $arItem['INPUT_NAME'] ?>"
                                       placeholder="<?= $arItem["NAME"] ?>">
                            </div>
                        </div>
                        <?
                        break;
                }
                ?>
            <? endif ?>
        <? endforeach; ?>


        <div class="col-xs-12">
            <div class="form-group text-center mb0">
                <input type="submit" name="set_filter" value="Искать" class="btn btn_gr"/>
            </div>
        </div>
        <input type="hidden" name="set_filter" value="Y"/>
    </div>
</form>

<script>
        $('select[name="arrFilter_pf[CITY]"]').styler({});
</script>