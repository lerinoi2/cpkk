<?php

function getFutureBusinessDay($num_business_days, $today_ymd = null, $holiday_dates_ymd = [])
{
    $num_business_days = min($num_business_days, 1000);
    $business_day_count = 0;
    $current_timestamp = empty($today_ymd) ? time() : strtotime($today_ymd);
    while ($business_day_count < $num_business_days) {
        $next1WD = strtotime('+1 weekday', $current_timestamp);
        $next1WDDate = date('Y-m-d', $next1WD);
        if (!in_array($next1WDDate, $holiday_dates_ymd)) {
            $business_day_count++;
        }
        $current_timestamp = $next1WD;
    }
    return date('Y-m-d', $current_timestamp);
}

$cities = [];

foreach ($arResult['DISPLAY_PROPERTIES']['CITY']['LINK_ELEMENT_VALUE'] as $city) {
    $cities[] = $city['NAME'];
}

$arResult['cities'] = $cities;

if ($arResult['cities'][0]) {
//    $city = $arResult['cities'][0];

    foreach ($arResult['cities'] as $ind => $city) {

        if ($arResult['DISPLAY_PROPERTIES']['PRICE_NEW']['VALUE']) {
            $p_city_id = null;
            foreach ($arResult['DISPLAY_PROPERTIES']['PRICE_NEW']['DESCRIPTION'] as $k => $price_c) {
                if ($price_c === $city) {
                    $p_city_id = $k;
                    break;
                }
            }
            if ($p_city_id !== null) {
                $arResult['PROPERTIES']['PRICE']['NEWVALUE'][$city] = $arResult['DISPLAY_PROPERTIES']['PRICE_NEW']['VALUE'][$p_city_id];
            }
            else {
                $arResult['PROPERTIES']['PRICE']['NEWVALUE'][$city] = $arResult['PROPERTIES']['PRICE']['VALUE'];
            }
        }

//        if ($arResult['DISPLAY_PROPERTIES']['DATE_START_NEW']['VALUE']) {
//            $date_start = '';
//            $dn_city_id = null;
//            foreach ($arResult['DISPLAY_PROPERTIES']['DATE_START_NEW']['DESCRIPTION'] as $ke => $dn_c) {
//                if ($dn_c === $city) {
//                    $dn_city_id = $ke;
//                    break;
//                }
//            }
//            if ($dn_city_id !== null) {
//                $date_start = $arResult['DISPLAY_PROPERTIES']['DATE_START_NEW'][$ind]['VALUE'][$dn_city_id];
//            }
//        }


        if ($arResult['PROPERTIES']['COUNT_DAY']['VALUE']) {
            $calendar = simplexml_load_string(file_get_contents('http://xmlcalendar.ru/data/ru/' . date('Y') . '/calendar.xml'));
            $calendar = $calendar->days->day;
            //все праздники за текущий год
            foreach ($calendar as $day) {
                $d = (array)$day->attributes()->d;
                $d = $d[0];
                $d = substr($d, 3, 2) . '.' . substr($d, 0, 2) . '.' . date('Y');
                //не считая короткие дни
                if ($day->attributes()->t == '1') {
                    $arHolidays[] = date('Y-m-d', strtotime($d));
                }
            }


            $arDate = [];
            foreach ($arResult['PROPERTIES']['DATE_START_NEW']['DESCRIPTION'] as $k => $c) {
                if ($c === $city) {
                    $arDATE_start = ParseDateTime($arResult['PROPERTIES']['DATE_START_NEW']['VALUE'][$k], FORMAT_DATETIME);
                    $date = $arDATE_start['YYYY'] . '-' . $arDATE_start['MM'] . '-' . $arDATE_start['DD'];
                    $q = getFutureBusinessDay($arResult['PROPERTIES']['COUNT_DAY']['VALUE'], $date, $arHolidays);
                    $arResult['PROPERTIES']['DATE_END_NEW']['NEWVALUE'][$city][$k] = date('d.m.Y', strtotime($q));
                }
            }
        }
    }
}


$i = 0;
foreach ($arResult['cities'] as $ind => $city) {
    if (!in_array($city, $arResult['PROPERTIES']['DATE_START_NEW']['DESCRIPTION'], true)) {
        $month6 = [
            '17.05.2021',
            '07.06.2021',
            '5.07.2021',
            '02.08.2021',
            '06.09.2021',
            '04.10.2021'
        ];
        foreach ($month6 as $k => $c) {
            $arResult['PROPERTIES']['DATE_START_NEW']['VALUE'][$i] = $c;
            $arResult['PROPERTIES']['DATE_START_NEW']['DESCRIPTION'][$i] = $city;
            $arDATE_start = ParseDateTime($c, FORMAT_DATETIME);
            $date = $arDATE_start['YYYY'] . '-' . $arDATE_start['MM'] . '-' . $arDATE_start['DD'];
            $q = getFutureBusinessDay($arResult['PROPERTIES']['COUNT_DAY']['VALUE'], $date, $arHolidays);
            $arResult['PROPERTIES']['DATE_END_NEW']['NEWVALUE'][$city][$i] = date('d.m.Y', strtotime($q));
            $i++;
        }
    }
}
