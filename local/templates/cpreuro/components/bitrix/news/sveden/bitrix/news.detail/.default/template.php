<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$page = $APPLICATION->GetCurPage();


//$dir    = $_SERVER['DOCUMENT_ROOT'].'/upload/iblock/93b/';
//$files1 = scandir($dir);
//$files2 = scandir($dir, 1);

//print_r($files1);
//print_r($files2);Q

//$filename =  $_SERVER['DOCUMENT_ROOT'].'/upload/iblock/93b/Лицензия на осуществление образовательной деятельности.pdf';
//genPdfThumbnail($filename,'/upload/PDFthumbnail/my.jpg');

//if (file_exists($filename)) {
//    echo "The file $filename exists";
//} else {
//    echo "The file $filename does not exist";
//}

//echo $_SERVER['DOCUMENT_ROOT'];

?>


<? if ($page === '/sveden/dokumenty/'): ?>
    <div class="tiny">
        <h1 style="text-align: center;">
            <?= $arResult['PROPERTIES']["TITLE"]["VALUE"]["TEXT"] ?>
        </h1>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12 newsPage__news">
                    <div class="documents_list">
                        <? foreach ($arResult['DISPLAY_PROPERTIES']['FILES']['FILE_VALUE'] as $file): ?>
                            <?
                            $file['FILE_NAME'] = explode('.', $file['FILE_NAME'])[0];
                            $type = pathinfo($file['SRC'], PATHINFO_EXTENSION);
                            ?>
                            <div class="col-md-4 document-item">
                                <a href="<?= $file['SRC'] ?>" download="">
                                    <img src="<?= $type === 'pdf' ? '/upload/medialibrary/0f3/0f367ab5ac9634c99c0ebcfe6cff65af.jpg' : $file['SRC'] ?>"
                                         alt="<?= $file['FILE_NAME'] ?>">
                                    <div class="title">
                                        <?= $file['FILE_NAME'] ?>
                                    </div>
                                    <div class="desc">
                                        <?= $file['DESCRIPTION'] ?>
                                    </div>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 newsPage__courses">
                    <aside>
                        <h2>Ближайшие курсы</h2>
                        <ul class="list-unstyled items_list">
                            <li><a href="/kursy/?city=1" class="btn btn_white">Пермь</a></li>
                            <li><a href="/kursy/?city=2" class="btn btn_white">Оса</a></li>
                            <li><a href="/kursy/?city=3" class="btn btn_white">Березники</a></li>
                            <li><a href="/kursy/?city=4" class="btn btn_white">Краснокамск</a></li>
                            <li><a href="/kursy/?city=5" class="btn btn_white">Кунгур</a></li>
                            <li><a href="/kursy/?city=6" class="btn btn_white">Киров</a></li>
                            <li><a href="/kursy/?city=7" class="btn btn_white">Усинск</a></li>
                            <li><a href="/kursy/?city=8" class="btn btn_white">Ижевск</a></li>
                            <li><a href="/kursy/?city=9" class="btn btn_white">Полазна</a></li>
                            <li><a href="/kursy/?city=10" class="btn btn_white">Чернушка</a></li>
                            <li><a href="/kursy/?city=11" class="btn btn_white">Оха</a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </div>
<? elseif ($page === '/sveden/blagodarstvennye-pisma/'): ?>
    <div class="tiny">
        <h1 style="text-align: center;">
            <?= $arResult['PROPERTIES']["TITLE"]["VALUE"]["TEXT"] ?>
        </h1>
        <div class="content">
            <div class="container">
                <div class="row">
                    <? foreach ($arResult['DISPLAY_PROPERTIES']['FILES']['FILE_VALUE'] as $file): ?>
                        <div class="col-md-3 gallery-item">
                            <a href="<?= $file['SRC'] ?>" data-fancybox="gallery"><img src="<?= $file['SRC'] ?>" alt=""></a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>

<? else: ?>

    <div class="tiny">
        <h1 style="text-align: center;">
            <?= $arResult['PROPERTIES']["TITLE"]["VALUE"]["TEXT"] ?>
        </h1>
        <?= $arResult['DETAIL_TEXT'] ?>
    </div>

<? endif; ?>