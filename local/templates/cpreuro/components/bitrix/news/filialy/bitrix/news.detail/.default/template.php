<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="tiny">
    <h1 style="text-align: justify;">
        <strong style="color: inherit; font-family: inherit; font-size: 30px; background-color: initial;">
            <?= $arResult['NAME'] ?>
        </strong>
    </h1>

    <?= $arResult['DETAIL_TEXT'] ?>

    <p class="mcetaggedbr">Руководитель филиала <strong><?= $arResult["PROPERTIES"]["FIO"]["VALUE"] ?></strong></p>
    <? if ($arResult["DETAIL_PICTURE"]["SAFE_SRC"]): ?>
        <p class="mcetaggedbr">
            <img src="<?= $arResult["DETAIL_PICTURE"]["SAFE_SRC"] ?>"
                 alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>" title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>">
        </p>
    <? endif; ?>
    <h3 class="mcetaggedbr">
        <strong>
            <a href="<?= CFile::GetPath($arResult["PROPERTIES"]["FILE"]["VALUE"]) ?>" target="_blank">
                Положение о филиале
            </a>
        </strong>
    </h3>


    <h3 class="mcetaggedbr">Схема проезда</h3>
    <p>
        <?
        $q = explode(',', $arResult["PROPERTIES"]['MAP']["VALUE"]);
        $ar = [
            'yandex_lat' => $q[0],
            'yandex_lon' => $q[1],
            'yandex_scale' => '17'
        ];
        $APPLICATION->IncludeComponent(
            "bitrix:map.yandex.view",
            "",
            Array(
                "API_KEY" => "fe86056f-dc3b-4791-8b68-8b727e42a609",
                "CONTROLS" => array("ZOOM", "MINIMAP", "TYPECONTROL", "SCALELINE"),
                "INIT_MAP_TYPE" => "MAP",
                "MAP_DATA" => serialize($ar),
                "MAP_HEIGHT" => "400",
                "MAP_ID" => "",
                "MAP_WIDTH" => "100%",
                "OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING")
            )
        ); ?>
    </p>
</div>
<a class="btn btn_gr" target="_blank" href="/kursy/?city=<?= $arResult["PROPERTIES"]['CITY']["VALUE"] ?>">Курсы в
    филиале</a>