<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div class="documents_list">
    <? if (count($arResult["SEARCH"]) > 0): ?>
    <? foreach ($arResult["SEARCH"] as $arItem): ?>
        <div class="courses_result__item">
            <p class="title"><?= $arItem["TITLE_FORMATED"] ?></p>
            <p class="date">по заявкам</p>
            <?= $arItem["BODY_FORMATED"] ?>
            <ul class="list-inline">
                <li><a href="<?= $arItem["URL_WO_PARAMS"] ?>" class="btn btn_white1 btn_sm">Подробнее</a>
                </li>
            </ul>
        </div>
    <? endforeach; ?>
</div>
<? if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"] ?>

<? else: ?>
    </div>
    <? ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND")); ?>
<? endif; ?>
