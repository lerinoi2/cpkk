<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="col-md-9 col-xs-6 stat">
    <div class="trigger_menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <ul class="list-inline main_navigation">
        <? if (!empty($arResult)): ?>
        <?
        $previousLevel = 0;
        foreach ($arResult as $arItem):?>
            <? if ($arItem['IS_PARENT']): ?>
                <li class="dropdown">
                <a href="#" data-toggle="dropdown" id="drop1"><?= $arItem["TEXT"] ?> <b class="caret"></b> </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
            <? else: ?>
                <? if ($arItem['DEPTH_LEVEL'] > 1): ?>
                    <? $previousLevel = $arItem['DEPTH_LEVEL']; ?>
                    <li>
                        <a role="menuitem" tabindex="-1" href="<?= $arItem["LINK"] ?>/"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? else: ?>
                    <? if ($previousLevel > 1): ?>
                        </ul>
                        </li>
                    <? endif; ?>
                    <? $previousLevel = $arItem['DEPTH_LEVEL']; ?>
                    <li>
                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? endif; ?>
            <? endif; ?>
        <? endforeach ?>
    </ul>
</div>
<? endif ?>
