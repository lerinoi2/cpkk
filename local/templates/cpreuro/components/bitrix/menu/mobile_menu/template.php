<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="col-md-12 col-xs-12 stat">
    <ul class="">
        <? if (!empty($arResult)): ?>
        <?
        $previousLevel = 0;
        foreach ($arResult as $arItem):?>
            <? if ($arItem['IS_PARENT']): ?>
                <li class="dropdown">
                <a href="#" data-toggle="dropdown" id="drop1"><?= $arItem["TEXT"] ?> <div class="arrow-menu">
                        <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.86293 6.67729L1.45706 0.140375C1.36539 0.0469228 1.2599 0 1.14083 0C1.02172 0 0.916279 0.0469228 0.824656 0.140375L0.137459 0.84181C0.0456912 0.935312 0 1.04272 0 1.16447C0 1.28623 0.0456912 1.39363 0.137459 1.48713L5.53971 7.00015L0.137459 12.5133C0.0456912 12.6068 0 12.7144 0 12.8358C0 12.9575 0.0456912 13.0651 0.137459 13.1586L0.824704 13.8598C0.916327 13.9534 1.02177 14 1.14088 14C1.25995 14 1.36539 13.9533 1.45706 13.8598L7.86259 7.32281C7.95416 7.22936 8 7.1217 8 7.00015C8 6.87859 7.9545 6.77099 7.86293 6.67729Z" fill="black"/>
                        </svg>
                    </div> </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                <li class="dropdown-back">
                    <span class="dropdown-back-title"><?= $arItem["TEXT"] ?></span>
                    <span class="dropdown-back-text">
                        <svg width="4" height="6" viewBox="0 0 4 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.0685363 3.1383L3.27147 5.93984C3.3173 5.97989 3.37005 6 3.42958 6C3.48914 6 3.54186 5.97989 3.58767 5.93984L3.93127 5.63922C3.97715 5.59915 4 5.55312 4 5.50094C4 5.44876 3.97715 5.40273 3.93127 5.36266L1.23015 2.99994L3.93127 0.637175C3.97715 0.597103 4 0.550988 4 0.498955C4 0.446796 3.97715 0.40068 3.93127 0.360608L3.58765 0.0600981C3.54184 0.0199842 3.48912 4.32174e-07 3.42956 4.26968e-07C3.37003 4.21763e-07 3.3173 0.0200261 3.27147 0.0600981L0.0687051 2.86165C0.0229178 2.9017 -2.10009e-07 2.94784 -2.14563e-07 2.99994C-2.19118e-07 3.05203 0.0227487 3.09815 0.0685363 3.1383Z" fill="black"/>
                        </svg>
                        Назад
                    </span>
                </li>
            <? else: ?>
                <? if ($arItem['DEPTH_LEVEL'] > 1): ?>
                    <? $previousLevel = $arItem['DEPTH_LEVEL']; ?>
                    <li>
                        <a role="menuitem" tabindex="-1" href="<?= $arItem["LINK"] ?>/"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? else: ?>
                    <? if ($previousLevel > 1): ?>
                        </ul>
                        </li>
                    <? endif; ?>
                    <? $previousLevel = $arItem['DEPTH_LEVEL']; ?>
                    <li>
                        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>
                <? endif; ?>
            <? endif; ?>
        <? endforeach ?>
    </ul>
</div>
<? endif ?>
