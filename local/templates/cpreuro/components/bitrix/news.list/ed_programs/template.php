<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container rel">
    <div class="row">
        <div class="col-xs-12">
            <h1>
                Образовательные программы
            </h1>
            <? //var_dump($arResult["ITEMS"]); ?>
            <?foreach($arResult["ITEMS"] as $arItem):?>
            	<?
                // echo '<pre>';
                // var_export($arItem['TIMESTAMP_X']);
                // echo '</pre>';
                $date = explode(' ', $arItem['TIMESTAMP_X']);

                $path = CFile::GetPath($arItem['PROPERTIES']['FILE']['VALUE']);
            	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            	?>
                <div class="section ">
                    <p>
                        <a class="doctitle" href="<?=$path?>"><?=$arItem['NAME']?></a>
                        <span class="date">Опубликовано <?=$date[0];?> | <?=substr($date[1], 0, 5);?></span>
                    </p>
                    <div class="file url-pdf">
                        <a href="<?=$path?>" download>Скачать файл</a>
                        <a href="http://docs.google.com/viewer?url=http://<?=$_SERVER['HTTP_HOST']?><?=$path?>&amp;embedded=true" target="_blank" class="gallery" >просмотреть</a>
                    </div>
                </div>
            <?endforeach;?>

            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                <br /><?=$arResult["NAV_STRING"]?>
            <?endif;?>

        </div>
    </div>
</div>
