<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["res"] as $arItem):?>
    <div class="col-md-6 col-xs-12 findProgram__item">
        <a href="<?=$arItem['LINK']?>" class="btn btn_white btn_industry">
            <span class="icon" style="background-image: url(<?=$arItem['PIC']?>)"></span>
            <?=$arItem['NAME']?>
        </a>
    </div>
<?endforeach;?>