<?

$spec = $arParams['SPEC'];
$rab = $arParams['RAB'];

$arRes = array();
$arFilter = Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE' => 'Y');
$arFilter['UF_INDEX'] = 1;
if ($spec) {
    $arFilter['UF_SPEC'] = 1;
}
if ($rab) {
    $arFilter['UF_RAB'] = 1;
}
$db_list = CIBlockSection::GetList(Array('SORT' => 'ASC'), $arFilter, true, array('NAME', 'PICTURE', 'SECTION_PAGE_URL', 'ID', 'UF_RAB', 'UF_SPEC', 'UF_INDEX'));
while ($ar_result = $db_list->GetNext()) {
    $arRes[] = [
        'NAME' => $ar_result['NAME'],
        'PIC' => CFile::GetPath($ar_result['PICTURE']),
        'LINK' => $ar_result['SECTION_PAGE_URL'],
        'ID' => $ar_result['ID']
    ];


}

$arResult['res'] = $arRes;

?>