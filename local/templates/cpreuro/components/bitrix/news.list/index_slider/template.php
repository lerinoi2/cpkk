<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>">
            <div class="main_slider__item">
                <div class="main_slider__bg">
                    <picture>
                        <source srcset="<?= CFile::GetPath($arItem['PROPERTIES']["IMG_PHONE"]["VALUE"]); ?>" media="(max-width: 767px)">
                        <source srcset="<?= CFile::GetPath($arItem['PROPERTIES']["IMG_TABLET"]["VALUE"]); ?>" media="(max-width: 1170px)">
                        <img src="<?= $arItem['PREVIEW_PICTURE']['SAFE_SRC'] ?>" alt="">
                    </picture>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-6 col-xs-12 col-xs-offset-0">
                            <div class="main_slider__inner">
                                <p class="title"><?= $arItem['NAME'] ?></p>
                                <div class="main_slider__text">
                                    <?= $arItem['PREVIEW_TEXT'] ?>
                                </div>
                                <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>">
                                    <div class="main_slider__button">
                                        <div class="main_slider__button_text">
                                            Подробнее
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
<? endforeach; ?>