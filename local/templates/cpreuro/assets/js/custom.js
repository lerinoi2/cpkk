$(document).ready(function () {

    $('.course__application form .form__btn .btn-blue.btn-md').click(function (e) {
        e.preventDefault();

    })


    $('#radio-2').click(function (e) {
        $('.city_choice').hide();
        $('.hour_och').hide();
        $('.hour_dist').show();
        $('.course__dates.och').hide();
        $('.course__dates.distant').show();

        $('.form__price.active').removeClass('active');
        $('.form__price.dist').addClass('active');
    })
    $('#radio-1').click(function (e) {
        $('.city_choice').show();
        $('.hour_och').show();
        $('.hour_dist').hide();
        $('.course__dates.och').show();
        $('.course__dates.distant').hide();
        $('.form__price.dist').removeClass('active');
        $('.form__price.och').addClass('active');

    })



    $('#orderkurs .form__btn').click(function (e) {
        e.preventDefault();

        let inputs = $('#orderkurs .form__inputs input'),
            err = 0;

        inputs.each(function (ind, el){
            var element = $(el).val();
            if (!element) {
                $(el).css('border-color', 'red');
                err++;
            } else  {
                $(el).css('border-color', 'unset');
            }
        });

        if (err === 0) {
            $('#exampleModalCenter').modal('toggle');

            setTimeout(function () {
                $('#exampleModalCenter').modal('hide');
            }, 2000);

        }

    })

    $('#consult .form__btn').click(function (e) {
        e.preventDefault();

        let inputs = $('#consult .form__inputs input'),
            err = 0;

        inputs.each(function (ind, el){
            var element = $(el).val();
            if (!element) {
                $(el).css('border-color', 'red');
                err++;
            } else  {
                $(el).css('border-color', 'unset');
            }
        });

        if (err === 0) {
            $('#exampleModalCenter').modal('toggle');

            setTimeout(function () {
                $('#exampleModalCenter').modal('hide');
            }, 2000);

        }

    })
    $('#questform .form__btn').click(function (e) {
        e.preventDefault();

        let inputs = $('#questform .form__inputs input'),
            err = 0;

        inputs.each(function (ind, el){
            var element = $(el).val();
            if (!element) {
                $(el).css('border-color', 'red');
                err++;
            } else  {
                $(el).css('border-color', 'unset');
            }
        });

        if (err === 0) {
            $('#exampleModalCenter').modal('toggle');

            setTimeout(function () {
                $('#exampleModalCenter').modal('hide');
            }, 2000);

        }

    })


    if ($('#up')[0] == undefined) {
        $('<div>', {
            id: "up",
            class: 'fa fa-angle-up',
            css: {
                color: '#fff',
                backgroundColor: '#009DFF',
                display: 'block',
                position: 'fixed',
                right: '7%',
                bottom: '10%',
                padding: '10px',
                border: "25px",
                borderRadius: '100%',
                textAlign: 'center',
                fontSize: '1.4em',
                cursor: 'pointer',
            },
            width: 40,
            height: 40,
            hover: function () {
                $(this).css("box-shadow", "0px 2px 3px 0px #009DFF");
            },
            mouseout: function () {
                $(this).css("box-shadow", "none");
            },
            on: {
                click: function (event) {
                    $("html, body").animate({scrollTop: 0}, 600);
                }
            },
        })
            .appendTo('body');
        $('#up').hide();

        $(window).scroll(function (event) {
            var top = $(window).scrollTop();
            if (top >= 200) {
                $('#up').fadeIn();
            } else {
                $('#up').fadeOut();
            }
        })
    }

    if ($('.main_slider').length) {
        $('.main_slider').slick({
            infinite: true,
            slidesToShow: 1,
        });
    }

    if ($('.feedback_slider').length) {
        $('.feedback_slider').slick({
            infinite: true,
            slidesToShow: 4,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,

                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,

                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,

                    }
                }
            ]
        });
    }

    $('.trigger_menu').click(function (event) {
        $('.main_navigation').slideToggle();
    });

    if ($('.datepicker').length) {
        $('.datepicker').datepicker({
            language: 'ru'
        });
    }


    $(function () {
        $('input[type="checkbox"], select').not(".styler-disabled").styler({});
    });

    $('#contact_form .submit').on('click', function (e) {
        e.preventDefault();
        var name = $('[name="name"]').val();
        var phone = $('[name="phone"]').val();
        var email = $('[name="email"]').val();
        var text = $('[name="text"]').val();

        if (!name || !phone) {
            $('#contact_form .email-status').html('Необходимо заполнить все поля');
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/ajax/callback.php',
            data: {
                name: name,
                phone: phone,
                email: email,
                text: text,
                action: 'mail'
            },
            success: function (data) {
                $('#contact_form .email-status').html('Ваше сообщение успешно отправлено!');
                $('#contact_form .submit').attr('disabled', 'true');
                // yaCounter51418078.reachGoal('send_letter');
            },
            error: function (xhr, str) {
                $('#contact_form .email-status').html('Возникла ошибка!');
            }
        });
    });

    $('.dist_btn').click(function () {
        track('apply_distance');
    });


    $('#modal_form #submit').on('click', function (e) {
        e.preventDefault();

        if (!$('form [type="checkbox"]').is(':checked')) {
            $('#modal_form .email-status').html('Необходимо дать согласие на обработку персональных данных');
            return;
        }
        if (!$('#modal_form .jq-checkbox').hasClass('checked')) {
            $('#modal_form .email-status').html('Необходимо дать согласие на обработку персональных данных');
            return;
        }

        var curse = $('[name="course"]').val();
        // var city = $('.city .jq-selectbox__select > .jq-selectbox__select-text').html();
        // var study = $('.study .jq-selectbox__select > .jq-selectbox__select-text').html();
        var name = $('[name="fio"]').val();
        var phone = $('#modal_form [name="phone"]').val();
        var email = $('[name="email"]').val();
        var text = $('[name="text"]').val();
        var lic = $('#modal_form .jq-checkbox');

        if (!curse) {
            $('#modal_form .email-status').html('Необходимо заполнить все поля');
            return;
        }

        if (!lic.hasClass('checked')) {
            $('#modal_form .email-status').html('Необходимо заполнить все поля');
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/ajax/request.php',
            data: {
                name: name,
                phone: phone,
                email: email,
                text: text,
                curse: curse,
                // city: city,
                // study: study,
                action: 'mail'
            },
            success: function (data) {
                $('#modal_form .email-status').html('Ваше сообщение успешно отправлено!');
                $('#modal_form #submit').attr('disabled', 'true');
                $('.modal-body h2').html('Ваше сообщение успешно отправлено!');
                $('.modal-body form').hide();

                setTimeout(function () {
                    $('.close').click();
                }, 2000);

                setTimeout(function () {
                    $('.modal-body h2').html('Заявка');
                    $('.modal-body form').show();
                    $('#modal_form #submit').removeAttr('disabled');
                    $('[name="fio"]').val('');
                    $('[name="phone"]').val('');
                    $('[name="email"]').val('');
                    $('[name="text"]').val('');
                }, 2000);
                // yaCounter51418078.reachGoal('ask_question');
                // Ya._metrika.counter.reachGoal('ask_question', {URL: document.location.href});
                // ga(ga.getAll()[0].get('name') + '.send', 'pageview', '/virtual/ask_question');
            },
            error: function (xhr, str) {
                $('#modal_form .email-status').html('Возникла ошибка!');
            }
        });
    });

    $('#modal_form_callback #submit').on('click', function (e) {
        e.preventDefault();

        if (!$('form [type="checkbox"]').is(':checked')) {
            $('#modal_form_callback .email-status').html('Необходимо дать согласие на обработку персональных данных');
            return;
        }

        var name = $('[name="fio_call"]').val();
        var phone = $('[name="phone_call"]').val();
        var email = $('[name="email_call"]').val();

        if (!phone) {
            $('#modal_form_callback .email-status').html('Заполните номера телефона, иначе не сможем Вам перезвонить');
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/ajax/callback.php',
            data: {
                name: name,
                phone: phone,
                email: email,
                action: 'callback'
            },
            success: function (data) {
                $('#modal_form_callback .email-status').html('Вам скоро перезвонят!');
                $('#modal_form_callback #submit').attr('disabled', 'true');
                $('.modal-body h2').html('Вам скоро перезвонят!');
                $('.modal-body form').hide();
                // Ya._metrika.counter.reachGoal('send_zayavka', {URL: document.location.href});
                // ga(ga.getAll()[0].get('name') + '.send', 'pageview', '/virtual/send_zayavka');
                setTimeout(function () {
                    $('.close').click();
                }, 2000);

                setTimeout(function () {
                    $('.modal-body h2').html('Заявка');
                    $('.modal-body form').show();
                    $('#modal_form_callback #submit').removeAttr('disabled');
                    $('[name="fio"]').val('');
                    $('[name="phone"]').val('');
                    $('[name="email"]').val('');
                }, 2000);
            },
            error: function (xhr, str) {
                $('#modal_form_callback .email-status').html('Возникла ошибка!');
            }
        });
    });

    $('#prof_select__input').change(function () {
        var val = $(this).val();
        val = val.replace(/\s+/, "");
        var link = $('#' + val).attr('src');
        location = link;
    });

    $('[data-target="#orderModal"]').on('click', function () {
        var name = $('h1').html();
        var city = $('[data-city]').attr('data-city');
        $('[name="course"]').val(name);
        $('.city').val(city);
        $('.jq-selectbox__select-text').html(city);
    });

    var tip_zav = $('#modal_form2').hasClass('dist_form');
    $('#submit2').on('click', function () {
        if(tip_zav == true) {
            track('form_distance_send');
        }
    });
    $('.courses_result__item [data-target="#orderModal"]').on('click', function () {
        var name = $(this).closest('.courses_result__item').find('.title').html();
        $('[name="course"]').val(name);
    });

    $('.dropdown-btn').on('click', function (e) {
        var check = $(this).find('.dropdown-men').is(':hidden');
        console.log(check);
        if (check) {
            $(this).find('.dropdown-men').show();
        } else {
            $(this).find('.dropdown-men').hide();

        }

    });

    // Yandex Metrika & GA

    // $('.list-inline .btn_gr').on('click', function () {
    //     if ($(this).text() == 'Подать заявку') {
    //         track('apply');
    //     }
    // });
    $('.form').on('submit', function () {
        track('send_application');
    });

    $('#contact_form').on('submit', function () {
        track('send_letter');
    });

//====================цели добавлены после переезда 01.2019=======================

    $('[data-target="#callModal"]').on('click', function () {
        ga('send', 'event', {
            eventCategory: 'click',
            eventAction: '/virtual/button_zayavka'
        });
        track('button_zayavka');
        console.log('reach.goal, button_zayavka');
    });
    $('#__submit').on('click', function () {
        ga('send', 'event', {
            eventCategory: 'submit',
            eventAction: '/virtual/send_zayavka'
        });

        track('send_zayavka');
        console.log('reach.goal, send_zayavka');
    });
    $('[data-target="#orderModal"]').on('click', function () {
        ga('send', 'event', {
            eventCategory: 'click',
            eventAction: '/virtual/apply'
        });
        track('apply');
        console.log('reach.goal, apply');
    });
    $('#submit').on('click', function () {
        ga('send', 'event', {
            eventCategory: 'submit',
            eventAction: '/virtual/ask_question'
        });
        track('form_send');
        console.log('reach.goal, form_send');
    });
    $('#contact_form .submit.btn.btn_gr').on('click', function () {
        ga('send', 'event', {
            eventCategory: 'submit',
            eventAction: '/virtual/send_letter'
        });
        track('send_letter');
        console.log('reach.goal, send_letter');
    });
    $('#course_moreinfo_form .btn-md').on('click', function () {
        ga('send', 'event', {
            eventCategory: 'submit',
            eventAction: '/virtual/realise_value_course'
        });
        track('realise_value_course');
        console.log('reach.goal, realise_value_course');
    });
	$('.kursi-capture-3__btn > button').on('click', function () {
        ga('send', 'event', {
            eventCategory: 'submit',
            eventAction: '/virtual/ask_question'
        });
        track('ask_question');
        console.log('reach.goal, ask_question');
	});
//================================================================================


    if ($("#kursi-page").length) {//#kursi-page only
        $('.js-reviews-slider').slick({
            arrows: true,
            dots: false,
            slidesToShow: 4,
            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1,
                        arrows: true,
                        dots: false,
                        adaptiveHeight: true
                    }
                }
            ]
        });

        $('#course_moreinfo_form, #course_questions_form').submit(function (e) {
            var $this_form = $(this);

            if (!$this_form.find('input[type="checkbox"]').is(':checked')) {
                alert('Необходимо дать согласие на обработку персональных данных');
                return false;
            }

            $.ajax({
                type: "POST",
                url: '/ajax/callback.php',
                data: $this_form.serialize(),
                success: function (response) {
                    $this_form.find('input[type=text], input[type=tel], textarea').val('');
                    alert("Отправлено успешно");

                    // ga('send', 'event', {
                    //     eventCategory: 'submit',
                    //     eventAction: '/virtual/ask_question'
                    // });
                    // track('ask_question');
                    // console.log('reach.goal, ask_question');


                },
                error: function (response) {
                    alert("Ошибка отправки");
                }
            });

            return false;
        });
    }

    // let dateNow = new Date();
    // let year = dateNow.getFullYear();
    // let mounth = dateNow.getMonth() + 1;
    // let day = dateNow.getDate();
    // let fullDate = day+'.'+mounth+'.'+year;
    // $('._startDate').daterangepicker({
    //     singleDatePicker: true,
    //     autoApply: true,
    //     autoUpdateInput: true,
    //     drops: 'down',
    //     opens: 'center',
    //     minDate: fullDate,
    //     locale: {
    //         format: 'DD.MM.YYYY',
    //         applyLabel: "Показать",
    //         cancelLabel: "Сбросить",
    //         "daysOfWeek": [
    //             "Вс",
    //             "Пн",
    //             "Вт",
    //             "Ср",
    //             "Чт",
    //             "Пт",
    //             "Сб"
    //         ],
    //         "monthNames": [
    //             "Январь",
    //             "Февраль",
    //             "Март",
    //             "Апрель",
    //             "Май",
    //             "Июнь",
    //             "Июль",
    //             "Август",
    //             "Сентябрь",
    //             "Октябрь",
    //             "Ноябрь",
    //             "Декабрь"
    //         ],
    //     }
    // }, function(start, end, label) {
    //     let startDate = $('._startDate').val(start.format('DD.MM.YYYY'));
    //     dateCourses();
    // });
    // $('._endDate').daterangepicker({
    //     singleDatePicker: true,
    //     autoApply: true,
    //     autoUpdateInput: true,
    //     drops: 'down',
    //     opens: 'center',
    //     minDate: fullDate,
    //     locale: {
    //         format: 'DD.MM.YYYY',
    //         applyLabel: "Показать",
    //         cancelLabel: "Сбросить",
    //         "daysOfWeek": [
    //             "Вс",
    //             "Пн",
    //             "Вт",
    //             "Ср",
    //             "Чт",
    //             "Пт",
    //             "Сб"
    //         ],
    //         "monthNames": [
    //             "Январь",
    //             "Февраль",
    //             "Март",
    //             "Апрель",
    //             "Май",
    //             "Июнь",
    //             "Июль",
    //             "Август",
    //             "Сентябрь",
    //             "Октябрь",
    //             "Ноябрь",
    //             "Декабрь"
    //         ],
    //     }
    // }, function(start, end, label) {
    //     let endDate = $('._endDate').val(start.format('DD.MM.YYYY'));
    //     dateCourses();
    // });
    // $('#all-date').on('change', function () {
    //     if ($(this).prop('checked')) {
    //         $('._startDate, ._endDate').attr('disabled', true);
    //         dateCourses();
    //     } else {
    //         $('._startDate, ._endDate').attr('disabled', false);
    //     }
    // });
    //
    // $('#all-courses').on('change', function () {
    //     if ($(this).prop('checked')) {
    //         $('.schedule__selects .select').addClass('disabled');
    //     } else {
    //         $('.schedule__selects .select').removeClass('disabled');
    //     }
    //     $('._elements .select__item input').map(function (index,item) {
    //         $(item).prop('checked', true);
    //     })
    // });
    //
    // $('#ochno').on('change', function () {
    //     if ($(this).prop('checked')) {
    //         $('.towns').show();
    //     }
    // })
    // $('#distance').on('change', function () {
    //     if ($(this).prop('checked')) {
    //         $('.towns').hide();
    //     };
    // })

    $('._moreInfo').on('click', function () {
        if ($(this).parent().hasClass('active')) {
            $('body, html').animate({scrollTop: $(this).parent().offset().top}, 500);
        }
        $(this).parent().toggleClass('active')
        let textBtn = $(this).parent().hasClass('active') ? 'Скрыть' : $(this).attr('data-text')
        $(this).text(textBtn)

    });

    $('._slickStudy').slick({
        variableWidth: true,
        arrows: true,
        slidesToShow: 1,
        dots: false,
        infinite: true,
        appendArrows: '._slickStudyArrow',
        prevArrow: '<div class="navArrows__item slick-prev"></div>',
        nextArrow: '<div class="navArrows__item slick-next"></div>',
    });
    $('._slickGratitude').slick({
        variableWidth: true,
        arrows: true,
        slidesToShow: 1,
        dots: false,
        infinite: false,
        appendArrows: '._slickGratitudeArrows',
        prevArrow: '<div class="navArrows__item slick-prev"></div>',
        nextArrow: '<div class="navArrows__item slick-next"></div>',
    });
    $('._sliderReviews').slick({
        variableWidth: true,
        arrows: true,
        slidesToShow: 1,
        dots: false,
        infinite: false,
        appendArrows: '._sliderReviewsArrows',
        prevArrow: '<div class="navArrows__item slick-prev"></div>',
        nextArrow: '<div class="navArrows__item slick-next"></div>',
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    variableWidth: false,
                }
            }
        ]
    })

    if ($(window).width() < 1170) {
        $('._advantagesSlider').slick({
            variableWidth: true,
            arrows: true,
            slidesToShow: 1,
            dots: false,
            infinite: false,
            appendArrows: '._advantagesSliderArrow',
            prevArrow: '<div class="navArrows__item slick-prev"></div>',
            nextArrow: '<div class="navArrows__item slick-next"></div>',
        });
        $('._accreditationSlider').slick({
            variableWidth: true,
            arrows: true,
            adaptiveHeight: true,
            slidesToShow: 1,
            dots: false,
            infinite: false,
            appendArrows: '._accreditationSliderArrow',
            prevArrow: '<div class="navArrows__item slick-prev"></div>',
            nextArrow: '<div class="navArrows__item slick-next"></div>',
        })
    }

    /* select */
    $('._chooseSelect').on('click', function () {
        let parent = $(this).parent();
        $(parent).toggleClass('active')
    });
    $('.select__item').on('click', function () {
        let parent = $(this).closest('.select');
        $(parent).find('.select__item').removeClass('active');
        $(this).addClass('active');
        $(parent).find('.select__selected span').text($(this).text());
        $(parent).toggleClass('active');

        var value = $(this).attr('value');
        if (value) {
            $('.form__price.active').removeClass('active');
            $('.form__price.active').removeClass('och');
            $('.form__price.'+value).addClass('active');
            $('.form__price.'+value).addClass('och');

            $('.date_range.active').removeClass('active');
            $('.city_'+value).addClass('active');

        }

    });
    /* end select */

    if ($("._customScroll").length) {
        $('._customScroll').mCustomScrollbar({
            axis: "y"
        });
    }
});

window.addEventListener('onBitrixLiveChat', function(event)
{
    var widget = event.detail.widget;

    // Обработка событий
    widget.subscribe({
        type: BX.LiveChatWidget.SubscriptionType.userMessage,
        callback: function (data) {

            track('bitrix_chat_send');

        }
    });
});

$('body').on('click', 'a.b24-widget-button-social-item.ui-icon.ui-icon-service-vk.connector-icon-45', function () {
    track('vk_chat_send');
});

$('._clear').on('click', function () {
    $('#contact_form .form-group input, #contact_form .form-group textarea').val('');
});
$('.mobileMenuButton').on('click', function () {
    $(this).toggleClass('active');
    $('.mobileMenu').toggleClass('active');
    $('body').toggleClass('overflow');
})




// ----------- Подстановка меню под высоту шапки -----------
if ($(window).width() < 1199)
{
    if ($('.siteHeader').length && $('.mobileMenu__search').length)
    {
        var header_height = parseInt($('.siteHeader').outerHeight() + $('.mobileMenu__search').outerHeight());
        $('.mobileMenu').css({'top': header_height + 'px'});
    }

}
// ----------- /Подстановка меню под высоту шапки -----------




function track(event) {
    if (event) {
        if ('undefined' !== typeof Ya && 'undefined' !== typeof Ya._metrika) {
            $.each(Ya._metrika.counters, function (key, counter) {
                counter.reachGoal(event, {URL: document.location.href});
                console.log('YM tracking: ' + event);
            });
        }
        if ('undefined' !== typeof ga) {
            ga(ga.getAll()[0].get('name')+'.send', 'pageview', '/virtual/' + event);
            console.log('GA tracking: ' + event);
        }
    }
}