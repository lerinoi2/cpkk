<?

use Bitrix\Main as Main;

global $assets;
$assets = Main\Page\Asset::getInstance();
?>
<!DOCTYPE html>
<html>
<head>
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle() ?></title>

    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">

    <meta property="og:title"
          content="Курсы повышения профессиональной квалификации в Перми: программы обучения для работников, специалистов и руководителей в образовательном центре АПО «НП Пермь-нефть»">
    <meta property="og:description"
          content="Курсы повышения квалификации, профессиональной переподготовки работников, специалистов и руководителей в центре АПО «НП Пермь-нефть». Выбрать программу обучения, узнать цены на услуги на сайте">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://cpkk.cpreuro.ru/">
    <meta property="og:image" content="/local/templates/cpreuro/assets/images/logo_big.png">
    <meta property="og:image:width" content="1383">
    <meta property="og:image:height" content="1124">
    <?
    $assets->addCss('/local/templates/cpreuro/assets/libs/jquery.fancybox.min.css');
    $assets->addCss('/local/templates/cpreuro/assets/css/style.css');
    $assets->addCss('/local/templates/cpreuro/assets/libs/bootstrap.min.css');
    $assets->addCss('/local/templates/cpreuro/assets/libs/font-awesome.min.css');
    $assets->addCss('/local/templates/cpreuro/assets/libs/slick.css');
    $assets->addCss('/local/templates/cpreuro/assets/libs/slick-theme.css');
    $assets->addCss('/local/templates/cpreuro/assets/libs/jquery.formstyler.css');
    $assets->addCss('/local/templates/cpreuro/assets/libs/jquery.formstyler.theme.css');
    $assets->addCss('/local/templates/cpreuro/assets/libs/customscroll/jquery.mCustomScrollbar.css');
    $assets->addCss('/local/templates/cpreuro/assets/css/custom.css');

    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/local/templates/cpreuro/assets/libs/bootstrap.min.js" async defer></script>
    <script src="/local/templates/cpreuro/assets/libs/slick.min.js" async defer></script>
    <script src="/local/templates/cpreuro/assets/libs/bootstrap-datepicker.js" async defer></script>
    <script src="/local/templates/cpreuro/assets/libs/bootstrap-datepicker.ru.js" async defer></script>
    <script src="/local/templates/cpreuro/assets/libs/jquery.formstyler.min.js"></script>
    <script src="/local/templates/cpreuro/assets/libs/jquery.fancybox.min.js" async defer></script>
    <script src="/local/templates/cpreuro/assets/libs/customscroll/jquery.mCustomScrollbar.concat.min.js" async defer></script>
    <script src="/local/templates/cpreuro/assets/js/custom.js" async></script>
    <script src="https://mc.yandex.ru/metrika/tag.js"></script>
    <script src="https://www.google-analytics.com/plugins/ua/linkid.js"></script>
    <script src="https://www.google-analytics.com/analytics.js"></script>
    <script src="https://lidrekon.ru/slep/js/uhpv-full.min.js"></script> <!-- скрипт для слабовидящих -->

    <?
    $APPLICATION->ShowLink('prev', 'prev');
    $APPLICATION->ShowLink('next', 'next');
    ?>


    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KVV2JN4');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KVV2JN4"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<a id="specialButton" href="#"><img src="https://lidrekon.ru/images/special.png" alt="ВЕРСИЯ ДЛЯ СЛАБОВИДЯЩИХ" title="ВЕРСИЯ ДЛЯ СЛАБОВИДЯЩИХ" /></a>
<header class="siteHeader">
    <div class="container">
        <div class="row mainMenu">
            <div class="mobileMenuButton col-md-1">
                <div></div>
            </div>
            <div class="col-lg-5 col-md-4 col-sm-5 col-xs-8 pr-0">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "standard.php",
                        "PATH" => "/include/head_img.php"
                    )
                ); ?>
            </div>
<!--            <div class="col-md-3 col-sm-6 col-xs-12">-->
<!--                <p class="header_text">Учебный центр</p>-->
<!--            </div>-->
            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 rightBlock">
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/head_contacts.php"
                        )
                    ); ?>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12 linkPlan">
                    <a href="/kursy/plan-grafik/">Скачать <br> План-график </a>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                    <a data-toggle="modal" data-target="#callModal" class="btn btn_gr">Перезвоните мне</a>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="mobileMenu__search">
    <div class="search_block pull-right">
        <? $APPLICATION->IncludeComponent(
            "bitrix:search.form",
            "head",
            array(
                "PAGE" => "#SITE_DIR#search/index.php",
                "USE_SUGGEST" => "N",
            ),
            false
        ); ?>
    </div>
</div>


<nav>
    <div class="container rel">
        <div class="row">
            <?php $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "head_menu",
                array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "sub",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "2",
                    "MENU_CACHE_GET_VARS" => '',
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "COMPONENT_TEMPLATE" => "horizontal_multilevel"
                ),
                false
            ); ?>
            <div class="col-md-3 col-xs-6">
                <div class="search_block pull-right">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:search.form",
                        "head",
                        array(
                            "PAGE" => "#SITE_DIR#search/index.php",
                            "USE_SUGGEST" => "N",
                        ),
                        false
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</nav>




<div class="mobileMenu">
    <div>
        <?php $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "mobile_menu",
            array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "sub",
                "DELAY" => "N",
                "MAX_LEVEL" => "2",
                "MENU_CACHE_GET_VARS" => '',
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "top",
                "USE_EXT" => "Y",
                "COMPONENT_TEMPLATE" => "horizontal_multilevel"
            ),
            false
        ); ?>
		<a href="/kursy/plan-grafik/" class="mobileMenu__grafik">
            <svg width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.61203 8.49023C8.47019 8.36865 8.30808 8.2876 8.12571 8.22681C7.94334 8.18628 7.7407 8.16602 7.49754 8.16602H6.56542C6.40332 8.16602 6.302 8.20654 6.22095 8.26733C6.16016 8.32812 6.11963 8.4497 6.11963 8.61181V11.2258C6.11963 11.3474 6.11963 11.4284 6.16016 11.5095C6.18042 11.5905 6.22095 11.6311 6.28174 11.6716C6.34253 11.7121 6.44384 11.7324 6.58569 11.7324H7.51781C7.67991 11.7324 7.82176 11.7121 7.9636 11.6919C8.08518 11.6716 8.20676 11.6311 8.32834 11.5905C8.44992 11.5297 8.55124 11.469 8.63229 11.3879C8.75387 11.2663 8.85519 11.1447 8.93624 11.0029C9.0173 10.8611 9.07809 10.6989 9.11862 10.5166C9.15914 10.3342 9.17941 10.1518 9.17941 9.92893C9.17941 9.30077 8.99703 8.81445 8.61203 8.49023ZM8.08518 11.0029C8.02439 11.0434 7.9636 11.0839 7.90281 11.1042C7.82176 11.1245 7.76097 11.1447 7.70018 11.1447C7.63939 11.1447 7.53807 11.1447 7.41649 11.1447H6.86938V8.71313H7.3557C7.5786 8.71313 7.76097 8.73339 7.92307 8.79418C8.08518 8.83471 8.20676 8.95629 8.30808 9.13866C8.4094 9.32103 8.47019 9.58446 8.47019 9.92893C8.44992 10.4558 8.32834 10.8003 8.08518 11.0029Z" fill="white"/>
                <path d="M15.0557 3.99189L15.0355 3.97163V3.95137L11.1854 0.101317L11.1651 0.0810537C11.1651 0.0810537 11.1449 0.0810537 11.1449 0.0607903C11.1449 0.0607903 11.1246 0.0607903 11.1246 0.0405268C11.0638 0 11.003 0 10.922 0H1.37791C1.01317 0 0.668693 0.141844 0.425532 0.385005L0.405269 0.405268C0.162107 0.64843 0 1.01317 0 1.37791V18.6221C0 19.0071 0.162107 19.3516 0.405269 19.5947C0.64843 19.8379 1.01317 20 1.37791 20H13.7183C14.1033 20 14.4478 19.8379 14.691 19.5947C14.9341 19.3516 15.0963 18.9868 15.0963 18.6221V4.19453C15.0963 4.11348 15.076 4.05269 15.0557 3.99189ZM11.307 1.45897L13.5968 3.74873H11.9149C11.7528 3.74873 11.5907 3.68794 11.4894 3.56636C11.388 3.46505 11.307 3.30294 11.307 3.14083V1.45897ZM14.2249 18.6221C14.2249 18.7639 14.1641 18.8855 14.0628 18.9868C13.9615 19.0881 13.8399 19.1489 13.6981 19.1489H1.37791C1.23607 19.1489 1.11449 19.0881 1.01317 19.0071C0.911854 18.9058 0.871327 18.7842 0.871327 18.6424V1.35765C0.871327 1.21581 0.932118 1.09422 1.03343 0.992908L1.0537 0.972644C1.15502 0.891591 1.2766 0.8308 1.39818 0.8308H10.4762V3.12057C10.4762 3.52584 10.6383 3.89058 10.922 4.17427C11.1854 4.43769 11.5704 4.5998 11.9757 4.5998H14.2249V18.6221Z" fill="white"/>
                <path d="M5.18747 8.4497C5.08615 8.34839 4.96457 8.2876 4.80247 8.24707C4.66062 8.20654 4.43772 8.18628 4.1743 8.18628H3.24218C3.08007 8.18628 2.95849 8.22681 2.8977 8.2876C2.81665 8.34839 2.79639 8.46997 2.79639 8.63207V11.3879C2.79639 11.5297 2.83691 11.6311 2.8977 11.7121C2.95849 11.7932 3.05981 11.8134 3.16113 11.8134C3.26245 11.8134 3.3435 11.7729 3.42455 11.6919C3.48534 11.6108 3.52587 11.5095 3.52587 11.3676V10.3545H4.19456C4.64036 10.3545 4.96457 10.2531 5.18747 10.0708C5.41037 9.88841 5.53195 9.60472 5.53195 9.23998C5.53195 9.07787 5.51169 8.91576 5.4509 8.77392C5.36984 8.65234 5.28879 8.53076 5.18747 8.4497ZM4.70115 9.58446C4.64036 9.66551 4.5593 9.7263 4.43772 9.76683C4.31614 9.80735 4.1743 9.82762 4.01219 9.82762H3.52587V8.73339H4.01219C4.33641 8.73339 4.53904 8.79418 4.66062 8.8955C4.76194 8.99682 4.80247 9.1184 4.80247 9.2805C4.7822 9.40208 4.76194 9.5034 4.70115 9.58446Z" fill="white"/>
                <path d="M10.2532 8.14575C10.1316 8.16602 10.0505 8.18628 9.98974 8.22681C9.92895 8.24707 9.86816 8.30786 9.8479 8.36865C9.80737 8.42944 9.80737 8.51049 9.80737 8.63207V11.3676C9.80737 11.5095 9.8479 11.6311 9.90869 11.6919C9.96948 11.7729 10.0708 11.7932 10.1721 11.7932C10.2734 11.7932 10.3545 11.7527 10.4355 11.6919C10.4963 11.6108 10.5369 11.5095 10.5369 11.3676V10.1924H11.7324C11.8337 10.1924 11.9148 10.1721 11.9756 10.1113C12.0363 10.0708 12.0566 9.98973 12.0566 9.90867C12.0566 9.82762 12.0363 9.76683 11.9756 9.70604C11.9148 9.66551 11.8337 9.62499 11.7324 9.62499H10.5369V8.71313H11.9553C12.0769 8.71313 12.1579 8.69287 12.2187 8.63207C12.2795 8.57128 12.2998 8.51049 12.2998 8.42944C12.2998 8.34839 12.2795 8.2876 12.2187 8.22681C12.1579 8.16601 12.0769 8.14575 11.9553 8.14575H10.2532Z" fill="white"/>
            </svg>
            <span>Скачать План-график</span>
        </a>
    </div>

    <div class="mobileMenu__contacts">
        <div class="header_links">
            <p>
                  <span class="out-circle">
                      <i class="fa fa-phone"></i>
                  </span>
                <a href="tel:+7(342) 282-05-86" class="callibri_phone">(342) 282-05-86</a>
            </p>
            <p>
                <i class="fa fa-envelope-o"></i>
                <a class="link" href="mailto:hline@ucpermoil.ru">hline@ucpermoil.ru</a>
            </p>
        </div>
    </div>
    <!--    <row>-->

    <!--    </row>-->
</div>