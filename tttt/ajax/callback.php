<?php
//define("NO_KEEP_STATISTIC", true);
//define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
CModule::IncludeModule('main');


if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (!empty($_POST['name']) and !empty($_POST['phone'])) {
        $el        = new CIBlockElement;
        $iblock_id = 11;
        $PROP      = array();

        $PROP[23]  = $_POST['phone'];
        $PROP[24]  = $_POST['email'];

        if (!empty($_POST['course']))
            $text2 = 'Курс - '.$_POST['course'];

        if ($_POST['form'] === 'course_moreinfo_form') {
            $PROP["FORM_NAME"] = 'Уточнить стоимость курса';
        } elseif ($_POST['form'] === 'course_questions_form') {
            $PROP["FORM_NAME"] = 'У вас остались вопросы?';
        } else if ($_POST['email'] !== null) {
            $PROP["FORM_NAME"] = 'Обратный звонок';
            $text2 = 'E-MAIL - '.$_POST['email'];
        }


        $fields = array(
            "IBLOCK_ID" => $iblock_id,
            "NAME" => $_POST['name'],
            'PREVIEW_TEXT' => $_POST['text'],
            "PROPERTY_VALUES" => $PROP,
        );


        if ($ID = $el->Add($fields)) {
            echo(" Сохранено \n");
            CEvent::SendImmediate
            (
                'FEEDBACK_FORM',
                's1',
                array(
                    'FORM_NAME' => $PROP["FORM_NAME"],
                    'AUTHOR' => $_POST['name'],
                    'AUTHOR_EMAIL' => $_POST['email'],
                    'TEXT' => $_POST['phone'],
                    'TEXT2' => $text2
                ),
                "N",
                7
            );
        }
    } else {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    }
}