<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "bread",
                        Array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0"
                        )
                    ); ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <h1>Поиск</h1>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:search.page",
                        "search",
                        Array(
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "CACHE_TIME" => "3600",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "N",
                            "DEFAULT_SORT" => "rank",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FILTER_NAME" => "",
                            "NO_WORD_LOGIC" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_TITLE" => "Результаты поиска",
                            "PAGE_RESULT_COUNT" => "10",
                            "RESTART" => "N",
                            "SHOW_WHEN" => "N",
                            "SHOW_WHERE" => "N",
                            "USE_LANGUAGE_GUESS" => "N",
                            "USE_SUGGEST" => "N",
                            "USE_TITLE_RANK" => "N",
                            "arrFILTER" => array(
                                0 => "iblock_courses",
                            ),
                            "arrFILTER_iblock_courses" => array(
                                0 => "all",
                            ),
                            "arrWHERE" => ""
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-4 col-xs-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/aside.php"
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>



<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>