<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

?>
    <div class="container">
        <div class="row" style="color: #213f50;">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div style="margin: 120px 0 0 0;"><img src="/local/templates/cpreuro/assets/images/logo404.png"></div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p style="font-size: 120px;">404</p>
                <h1 style="text-transform: none;">К сожалению, страница не найдена</h1>
                <h2 style="text-transform: none; font-size: 21px; margin: 60px 0 20px 0;">Возможно, Вас заинтересуют
                    разделы:</h2>
                <a style="color: #007CDD; font-size:18px;" href="/">Главная</a><br>
                <a style="color: #007CDD; font-size:18px;" href="/novosti/">Новости</a><br>
                <a style="color: #007CDD; font-size:18px;" href="/sveden/common/" data-toggle="dropdown" id="drop1">Сведения
                    об образовательной организации</a><br>
                <a style="color: #007CDD; font-size:18px;" href="/kursy/">Обучение</a><br>
                <a style="color: #007CDD; font-size:18px;" href="/kontakty/">Контакты</a>
            </div>
        </div>
    </div>
<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>