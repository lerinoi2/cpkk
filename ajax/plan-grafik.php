<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

$newStartDate = $_POST['startDate'] !== '' ? ConvertDateTime($_POST['startDate'], "YYYY-MM-DD") : '';
$newEndDate = ConvertDateTime($_POST['endDate'], "YYYY-MM-DD");
$allDate = $_POST['allDate'] === 'true' ? true : false;
$sectionList = [];
$elementList = [];
$allData = [];

// получение курсов курсов
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
if ($newStartDate) {
    $arFilter['>=PROPERTY_DATE_START_NEW'] = $newStartDate;
}
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();

    if ($_POST['city'] != '') { // если был выбран город/города (очный формат)
        for ($c = 0; $c <= count($_POST['city']); $c++) { // цикл по выбранным городам
            for ($i = 0; $i <= count($arProps['DATE_START_NEW']["VALUE"]); $i++) { // цикл по датам курса
                $dateStart = ConvertDateTime($arProps['DATE_START_NEW']["VALUE"][$i], "YYYY-MM-DD"); // конверт даты
                if ($allDate) { // если стоит галка "все даты"
                    if ($arProps['DATE_START_NEW']["DESCRIPTION"][$i] == $_POST['city'][$c]) { // проверка курсов на выбранные город
                        if ($dateStart >= $newStartDate) { // если начало курса с сегодня
                            $elementList[] = [
                                "field" => $arFields,
                                "props" => $arProps
                            ];
                        }
                    }
                } else {
                    if ($arProps['DATE_START_NEW']["DESCRIPTION"][$i] == $_POST['city'][$c]) { //проверка курсов на выбранные город
                        if ($dateStart >= $newStartDate && date('Y-m-d', strtotime($dateStart . '+ ' . $arProps['COUNT_DAY']["VALUE"] . ' days')) <= $newEndDate) { // если пользователь выбрал даты смотрим сегодня и подсчитываем дату конца курса
                            $elementList[] = [
                                "field" => $arFields,
                                "props" => $arProps
                            ];
                        }
                    }
                }
            }
        }
    } else { // не указан город (дистанционный формат)
        for ($i = 0; $i <= count($arProps['DATE_START_NEW']["VALUE"]); $i++) {
            if ($arProps['DISTANCE']["VALUE"] === 'Да' && $arProps['DATE_START_NEW']["DESCRIPTION"][$i] === 'Пермь') { // Проверяем есть ли у курса такой формат
                $dateStart = ConvertDateTime($arProps['DATE_START_NEW']["VALUE"][$i], "YYYY-MM-DD");
                if ($allDate) { // если стоит галка "все даты"
                    if ($dateStart >= $newStartDate) {
                        $elementList[] = [
                            "field" => $arFields,
                            "props" => $arProps
                        ];
                    }
                } elseif ($dateStart >= $newStartDate && date('Y-m-d', strtotime($dateStart.'+ '.$arProps['COUNT_DAY']["VALUE"].' days')) <= $newEndDate) { // если пользователь выбрал даты смотрим сегодня и подсчитываем дату конца курса
                    $elementList[] = [
                        "field" => $arFields,
                        "props" => $arProps
                    ];
                }
            }
        }
    }
}

$arAllSelect = Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PROPERTY_*");
//var_dump($_POST['codeCity']);
//var_dump('test');
if ($_POST['codeCity'] === NULL) {
    $arAllFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_CITY" => '25', "PROPERTY_DATE_START_NEW" => '');
} else {
    $arAllFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_CITY" => $_POST['codeCity']['id'], "PROPERTY_DATE_START_NEW" => '');
}
$resAll = CIBlockElement::GetList(Array(), $arAllFilter, false, false, $arAllSelect);
//$elementListt = [];
//print_r($_POST['codeCity']['id']);
while($obAll = $resAll->GetNextElement()) {
    $arAllFields = $obAll->GetFields();
    $arAllProps = $obAll->GetProperties();

    if ($_POST['codeCity'] === NULL) {
//        print_r($arAllFields);
//        print_r($arAllProps);
        if ($arAllProps['DISTANCE']["VALUE"] === 'Да' && count($arAllProps['DATE_START_NEW']["DESCRIPTION"]) == 1) {
            $elementList[] = [
                "field" => $arAllFields,
                "props" => $arAllProps
            ];
        }
    } else {
        if (count($arAllProps['DATE_START_NEW']["DESCRIPTION"]) == 1) {
            $elementList[] = [
                "field" => $arAllFields,
                "props" => $arAllProps
            ];
        }
    }
//    }
}

//var_dump($elementList);

foreach ($elementList as $section) {
    $res = CIBlockSection::GetByID($section["field"]["IBLOCK_SECTION_ID"]);
    if($ar_res = $res->GetNext())
        $sectionList[] = [
            'id' => $ar_res['ID'],
            'name' => $ar_res['NAME']
        ];
}

//$sectionList = array_diff($sectionList, array_diff_assoc($sectionList, array_unique($sectionList)));
//$sectionList = array_unique($sectionList);

$allData = [
    'sections' => $sectionList,
    'elements' => $elementList
];

echo json_encode($allData);



