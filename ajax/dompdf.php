<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

require_once ($_SERVER['DOCUMENT_ROOT'].'/lib/dompdf/autoload.inc.php');

use Dompdf\Dompdf;
use Dompdf\Options;

$newStartDate = $_POST['startDate'] !== '' ? ConvertDateTime($_POST['startDate'], "YYYY-MM-DD") : date('Y-m-d');
$newEndDate = ConvertDateTime($_POST['endDate'], "YYYY-MM-DD");
$allDate = $_POST['allDate'] === 'true' ? true : false;
$allData = [];
$elementList = [];
$table = '';
$order = '';

// получение курсов курсов
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PROPERTY_*");
if ($_POST['allCourses'] === 'true' ) {
    $arFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", '>=PROPERTY_DATE_START_NEW' => $newStartDate, 'NAME' => $_POST['courses']);
} else {
    $arFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", '>=PROPERTY_DATE_START_NEW' => $newStartDate, 'NAME' => $_POST['courses'], 'IBLOCK_SECTION_ID' => $_POST['napravlenie']);
}

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();

    if ($_POST['city'] != '') { // если указан город (очный формат)
        for ($c = 0; $c <= count($_POST['city']); $c++) { // цикл по выбранным городам
            for ($i = 0; $i <= count($arProps['DATE_START_NEW']["VALUE"]); $i++) {
                $dateStart = ConvertDateTime($arProps['DATE_START_NEW']["VALUE"][$i], "YYYY-MM-DD");
                if ($allDate) { // если стоит галка "все даты"
                    if ($arProps['DATE_START_NEW']["DESCRIPTION"][$i] == $_POST['city'][$c]) { // проверка курсов на выбранные город
                        if ($dateStart >= $newStartDate) { // если начало курса с сегодня
                            $price = $arProps['PRICE']['VALUE'];
                            if (count($arProps['PRICE_NEW']["VALUE"]) > 0) {
                                for ($j = 0; $j <= count($arProps['PRICE_NEW']["VALUE"]); $j++) {
                                    if ($arProps['PRICE_NEW']["DESCRIPTION"][$j] == $_POST['city'][$c]) {
                                        $price = $arProps['PRICE_NEW']["VALUE"][$j];
                                    }
                                }
                            }
                            if ($arProps['COUNT_DAY']["VALUE"] > 0) {
                                $finishCourse = date('d.m.Y', strtotime($dateStart . '+' . $arProps['COUNT_DAY']["VALUE"] . ' days'));
                            }
                            $table = $table . '
                                    <tr style="font-size: 10px; text-align: left">
                                        <td><a href="https://cpkk.cpreuro.ru'.$arFields['DETAIL_PAGE_URL'].'">'.$arFields['NAME'].'</a> </td>
                                        <td >' . $arProps['HOURS']['VALUE'] . '</td>
                                        <td >' . $arProps['DATE_START_NEW']["VALUE"][$i].' - '.$finishCourse. '</td>
                                        <td >' . $arProps['DATE_START_NEW']["DESCRIPTION"][$i] . '</td>
                                        <td >'. $price .'</td>
                                    </tr>
                                ';
                        }
                    }
                } else {
                    if ($arProps['DATE_START_NEW']["DESCRIPTION"][$i] == $_POST['city'][$c]) { //проверка курсов на выбранные город
                        if ($dateStart >= $newStartDate && date('Y-m-d', strtotime($dateStart . '+ ' . $arProps['COUNT_DAY']["VALUE"] . ' days')) <= $newEndDate) { // если пользователь выбрал даты смотрим сегодня и подсчитываем дату конца курса
                            if (count($arProps['PRICE_NEW']["VALUE"]) > 0) {
                                for ($j = 0; $j <= count($arProps['PRICE_NEW']["VALUE"]); $j++) {
                                    if ($arProps['PRICE_NEW']["DESCRIPTION"][$j] == $_POST['city'][$c]) {
                                        $price = $arProps['PRICE_NEW']["VALUE"][$j];
                                    }
                                }
                            }
                            $finishCourse = date('d.m.Y', strtotime($dateStart . '+' . $arProps['COUNT_DAY']["VALUE"] . ' days'));
                            $table = $table . '
                                    <tr style="font-size: 10px; text-align: left">
                                        <td><a href="https://cpkk.cpreuro.ru'.$arFields['DETAIL_PAGE_URL'].'">'.$arFields['NAME'].'</a></td>
                                        <td >' . $arProps['HOURS']['VALUE'] . '</td>
                                        <td >' . $arProps['DATE_START_NEW']["VALUE"][$i].' - '.$finishCourse. '</td>
                                        <td >' . $arProps['DATE_START_NEW']["DESCRIPTION"][$i] . '</td>
                                        <td >' . $price . '</td>
                                    </tr>
                                ';
                        }
                    }
                }
            }
        }
    } else { // не указан город (дистанционный формат)
        for ($i = 0; $i <= count($arProps['DATE_START_NEW']["VALUE"]); $i++) {
            if ($arProps['DISTANCE']["VALUE"] === 'Да' && $arProps['DATE_START_NEW']["DESCRIPTION"][$i] === 'Пермь') { // Проверяем есть ли у курса такой формат
                $dateStart = ConvertDateTime($arProps['DATE_START_NEW']["VALUE"][$i], "YYYY-MM-DD");
                if ($allDate) { // если стоит галка "все даты"
                    if ($dateStart >= $newStartDate) {
                        $price = $arProps['PRICE']['VALUE'];
                        if (count($arProps['PRICE_NEW']["VALUE"]) > 0) {
                            for ($j = 0; $j <= count($arProps['PRICE_NEW']["VALUE"]); $j++) {
                                if ($arProps['PRICE_NEW']["DESCRIPTION"][$j] == 'Пермь') {
                                    $price = $arProps['PRICE_NEW']["VALUE"][$j];
                                }
                            }
                        }
                        $finishCourse = date('d.m.Y', strtotime($dateStart . '+' . $arProps['COUNT_DAY']["VALUE"] . ' days'));
                        $table = $table.'
                                <tr style="font-size: 10px; text-align: left">
                                    <td><a href="https://cpkk.cpreuro.ru'.$arFields['DETAIL_PAGE_URL'].'">'.$arFields['NAME'].'</a></td>
                                    <td>'.$arProps['HOURS']['VALUE'].'</td>
                                    <td >' . $arProps['DATE_START_NEW']["VALUE"][$i].' - '.$finishCourse. '</td>
                                    <td>'.$arProps['DATE_START_NEW']["DESCRIPTION"][$i].'</td>
                                    <td>'.$price.'</td>
                                </tr>
                            ';
                    }
                } elseif ($dateStart >= $newStartDate && date('Y-m-d', strtotime($dateStart.'+ '.$arProps['COUNT_DAY']["VALUE"].' days')) <= $newEndDate) { // если пользователь выбрал даты смотрим сегодня и подсчитываем дату конца курса
                    $price = $arProps['PRICE']['VALUE'];
                    if (count($arProps['PRICE_NEW']["VALUE"]) > 0) {
                        for ($j = 0; $j <= count($arProps['PRICE_NEW']["VALUE"]); $j++) {
                            if ($arProps['PRICE_NEW']["DESCRIPTION"][$j] == 'Пермь') {
                                $price = $arProps['PRICE_NEW']["VALUE"][$j];
                            }
                        }
                    }
                    $finishCourse = date('d.m.Y', strtotime($dateStart . '+' . $arProps['COUNT_DAY']["VALUE"] . ' days'));
                    $table = $table.'
                                <tr style="font-size: 10px; text-align: left">
                                    <td><a href=https://cpkk.cpreuro.ru'.$arFields['DETAIL_PAGE_URL'].'">'.$arFields['NAME'].'</a> </td>
                                    <td>'.$arProps['HOURS']['VALUE'].'</td>
                                    <td >' . $arProps['DATE_START_NEW']["VALUE"][$i].' - '.$finishCourse. '</td>
                                    <td>'.$arProps['DATE_START_NEW']["DESCRIPTION"][$i].'</td>
                                    <td>'.$price.'</td>
                                </tr>
                            ';
                }
            }
        }
    }
}


$arAllSelect = Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PROPERTY_*");
if ($_POST['codeCity'] === NULL) {
    $arAllFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_CITY" => '25', "PROPERTY_DATE_START_NEW" => '', 'NAME' => $_POST['courses'], 'IBLOCK_SECTION_ID' => $_POST['napravlenie']);
} else {
    $arAllFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_CITY" => $_POST['codeCity']['id'], "PROPERTY_DATE_START_NEW" => '', 'NAME' => $_POST['courses'], 'IBLOCK_SECTION_ID' => $_POST['napravlenie']);
}

$resAll = CIBlockElement::GetList(Array(), $arAllFilter, false, false, $arAllSelect);


while($obAll = $resAll->GetNextElement()) {
    $arAllFields = $obAll->GetFields();
    $arAllProps = $obAll->GetProperties();

//    print_r($arAllProps);
    if ($_POST['codeCity'] === NULL) {
        $price = $arAllProps['PRICE']['VALUE'];
        if (count($arAllProps['PRICE_NEW']["VALUE"]) > 1) {
            for ($j = 0; $j <= count($arAllProps['PRICE_NEW']["VALUE"]); $j++) {
                if ($arAllProps['PRICE_NEW']["DESCRIPTION"][$j] == $_POST['city'][$c]) {
                    $price = $arAllProps['PRICE_NEW']["VALUE"][$j];
                }
            }
        } else {
            $price = $arAllProps['PRICE']["VALUE"];
        }
//        print_r(count($arAllProps['PRICE_NEW']["VALUE"]));
//        print_r($arAllProps);
        if (count($arAllProps['DATE_START_NEW']["DESCRIPTION"]) == 1 && $arAllProps['DISTANCE']["VALUE"] === 'Да') {
            $order = $order . '
                <tr style="font-size: 10px; text-align: left">
                    <td><a href="https://cpkk.cpreuro.ru'.$arAllFields['DETAIL_PAGE_URL'].'">' . $arAllFields['NAME'] . '</a></td>
                    <td >' . $arAllProps['HOURS']['VALUE'] . '</td>
                    <td >По заявкам</td>
                    <td >' . $_POST['codeCity']['name'][$c] . '</td>
                    <td>'.$price.'</td>
                </tr>
            ';
        }
    } else {
        for ($c = 0; $c < count($_POST['codeCity']['name']); $c++) {
            for ($i = 0; $i <= count($arAllProps['CITY']['VALUE']); $i++) {
                if ($_POST['codeCity']['id'][$c] == $arAllProps['CITY']['VALUE'][$i]) {
                    if (count($arAllProps['DATE_START_NEW']["DESCRIPTION"]) == 1) {
                        $price = $arAllProps['PRICE']['VALUE'];
                        if (count($arAllProps['PRICE_NEW']["VALUE"]) > 1) {
                            for ($j = 0; $j <= count($arAllProps['PRICE_NEW']["VALUE"]); $j++) {
                                if ($arAllProps['PRICE_NEW']["DESCRIPTION"][$j] == $_POST['city'][$c]) {
                                    $price = $arAllProps['PRICE_NEW']["VALUE"][$j];
                                }
                            }
                        } else {
                            $price = $arAllProps['PRICE']["VALUE"];
                        }
                        $order = $order . '
                            <tr style="font-size: 10px; text-align: left">
                                <td><a href="https://cpkk.cpreuro.ru'.$arAllFields['DETAIL_PAGE_URL'].'">' . $arAllFields['NAME'] . '</a></td>
                                <td >' . $arAllProps['HOURS']['VALUE'] . '</td>
                                <td >По заявкам</td>
                                <td >' . $_POST['codeCity']['name'][$c] . '</td>
                                <td>'.$price.'</td>
                            </tr>
                        ';
                    }
                }
            }
        }
    }
}

//буферизируем вывод
ob_start();
?>
    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <title>Расписание курсов</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            /*Отступы на самой PDF странице:*/
            @page { margin: 10px 50px; }
            /*стили...*/
            .header {
                clear: both;
            }
            .header a {
                color: #000;
                text-decoration: none;
            }
            .header__logo {
                float: left;
                padding-left: 72px;
                position: relative;
            }
            .header__logo:before {
                content: "";
                position: absolute;
                top: 0;
                left: 0;
                width: 62px;
                height: 61px;
                background: url(/local/templates/cpreuro/assets/images/logo.png) no-repeat center left;
            }
            .header__logo span {
                display: block;
            }
            .header__info {
                float: right;
            }
            .logo_title {
                text-transform: uppercase;
            }
            .mainInfo {
                clear: both;
            }
            table {
                font-size: 14px;
            }
            td {
                padding: 5px;
            }
        </style>
    </head>
    <body>
            <div class="header">
                <a href="https://cpkk.cpreuro.ru/" class="header__logo">
                    <span class="logo_title">АПО "НП Пермь-нефть"</span>
                    <span>работаем с 1942 года</span>
                </a>
                <div class="header__info">
                    <div><a href="tel:+73422820586" class="callibri_phone">+7(342) 282-05-86</a></div>
                    <div><a class="link" href="mailto:hline@ucpermoil.ru">hline@ucpermoil.ru</a></div>
                    <div><a class="link" href="https://cpkk.cpreuro.ru/">cpkk.cpreuro.ru</a></div>
                </div>
            </div>

    <!--содержимое body-->
    <div class="mainInfo">
        <h1 style="text-align: center" >Расписание курсов</h1>
        <div class="mainInfo__info">
            <div>Формат: <?= $_POST['format'] === 'true' ? 'Очный' : 'Дистанционный'?></div>
            <div>Город: <?= implode(',', $_POST['city']) ?></div>
            <div>Дата:  <?= $allDate ? 'Все даты' : ''.$_POST['startDate'].' - '.$_POST['endDate']?> </div>
        </div>
        <br><br>
        <table border="1" style="border-collapse: collapse; width: 100%">
            <thead>
            <tr style="font-size: 12px; text-align: center">
                <td> Курс </td>
                <td> Кол-во часов </td>
                <td> Дата начала </td>
                <td> Город </td>
                <td> Цена </td>
            </tr>
            </thead>
            <tbody>
            <?=$table?>
            <? if ($order) {?>
                <tr>
                    <td colspan="5">Курсы по заявкам</td>
                </tr>
                <?=$order?>
            <? } ?>
            </tbody>
        </table>
    </div>
    <br><br>
    <div>Дата формирования документа: <?= date("d.m.yy")?> </div>
    </body>
    </html>
<?

$html = ob_get_clean(); //останавливаем буферизацию, запихиваем буфер в $html

$options = new Options();
$options->set('defaultFont', 'roboto');

$dompdf = new Dompdf($options);


$dompdf->loadHtml($html, 'UTF-8');
//ставим A4 вертикально
$dompdf->setPaper('A4', 'portrait');
// Render the HTML as PDF - рендерим
$dompdf->render();
// Output the generated PDF to Browser
//$dompdf->stream('test.pdf',array('Attachment'=> 1 ));

//$dompdf->output();
//$output = $dompdf->output();
$number = rand(100000, 999999999);

//ob_clean();

//$file_location = $_SERVER['DOCUMENT_ROOT']."/pdf/test.pdf";
//$file_location = $_SERVER['DOCUMENT_ROOT']."/pdf/test".$number.".pdf";
//file_put_contents($file_location,$output);
//echo "/pdf/test".$number.".pdf";



file_put_contents($_SERVER['DOCUMENT_ROOT']."/pdf/{$number}.pdf", $dompdf->output());
echo "/pdf/".$number.".pdf";


