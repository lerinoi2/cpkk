<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');


require_once '/var/www/u0551855/data/www/cpkk.testck.ru/lib/tcpdf_min/tcpdf.php';

$newStartDate = $_POST['startDate'] !== '' ? ConvertDateTime($_POST['startDate'], "YYYY-MM-DD") : date('Y-m-d');
$newEndDate = ConvertDateTime($_POST['endDate'], "YYYY-MM-DD");
$allDate = $_POST['allDate'] === 'true' ? true : false;
$allData = [];
$elementList = [];
$table = '';
// получение курсов курсов
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", '>=PROPERTY_DATE_START_NEW' => $newStartDate, 'NAME' => $_POST['courses'], 'IBLOCK_SECTION_ID' => $_POST['napravlenie']);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();

    if ($_POST['city'] != '') { // если указан город (очный формат)
        for ($c = 0; $c <= count($_POST['city']); $c++) { // цикл по выбранным городам
            for ($i = 0; $i <= count($arProps['DATE_START_NEW']["VALUE"]); $i++) {
                $dateStart = ConvertDateTime($arProps['DATE_START_NEW']["VALUE"][$i], "YYYY-MM-DD");
                if ($allDate) { // если стоит галка "все даты"
                    if ($arProps['DATE_START_NEW']["DESCRIPTION"][$i] == $_POST['city'][$c]) { // проверка курсов на выбранные город
                        if ($dateStart >= $newStartDate) { // если начало курса с сегодня
                            $table = $table . '
                                    <tr style="font-size: 10px; text-align: left">
                                        <td >' . $arFields['NAME'] . ' <br> ' . $arProps['PRICE']['VALUE'] . ' </td>
                                        <td >' . $arProps['HOURS']['VALUE'] . '</td>
                                        <td >' . $arProps['DATE_START_NEW']["VALUE"][$i] . '</td>
                                        <td >' . $arProps['DATE_START_NEW']["DESCRIPTION"][$i] . '</td>
                                        <td >' . $arProps['COUNT_DAY']["VALUE"] . '</td>
                                    </tr>
                                ';
                        }
                    }
                } else {
                    if ($arProps['DATE_START_NEW']["DESCRIPTION"][$i] == $_POST['city'][$c]) { //проверка курсов на выбранные город
                        if ($dateStart >= $newStartDate && date('Y-m-d', strtotime($dateStart . '+ ' . $arProps['COUNT_DAY']["VALUE"] . ' days')) <= $newEndDate) { // если пользователь выбрал даты смотрим сегодня и подсчитываем дату конца курса
                            $table = $table . '
                                    <tr style="font-size: 10px; text-align: left">
                                        <td >' . $arFields['NAME'] . ' <br> ' . $arProps['PRICE']['VALUE'] . ' </td>
                                        <td >' . $arProps['HOURS']['VALUE'] . '</td>
                                        <td >' . $arProps['DATE_START_NEW']["VALUE"][$i] . '</td>
                                        <td >' . $arProps['DATE_START_NEW']["DESCRIPTION"][$i] . '</td>
                                        <td >' . $arProps['COUNT_DAY']["VALUE"] . '</td>
                                    </tr>
                                ';
                        }
                    }
                }
            }
        }
    } else { // не указан город (дистанционный формат)
        for ($i = 0; $i <= count($arProps['DATE_START_NEW']["VALUE"]); $i++) {
            if ($arProps['DISTANCE']["VALUE"] === 'Да' && $arProps['DATE_START_NEW']["DESCRIPTION"][$i] === 'Пермь') { // Проверяем есть ли у курса такой формат
                $dateStart = ConvertDateTime($arProps['DATE_START_NEW']["VALUE"][$i], "YYYY-MM-DD");
                if ($allDate) { // если стоит галка "все даты"
                    if ($dateStart >= $newStartDate) {
                        $table = $table.'
                                <tr style="font-size: 10px; text-align: left">
                                    <td>'.$arFields['NAME'].' <br> '.$arProps['PRICE']['VALUE'].' </td>
                                    <td>'.$arProps['HOURS']['VALUE'].'</td>
                                    <td>'.$arProps['DATE_START_NEW']["VALUE"][$i].'</td>
                                    <td>'.$arProps['DATE_START_NEW']["DESCRIPTION"][$i].'</td>
                                    <td>'.$arProps['COUNT_DAY']["VALUE"].'</td>
                                </tr>
                            ';
                    }
                } elseif ($dateStart >= $newStartDate && date('Y-m-d', strtotime($dateStart.'+ '.$arProps['COUNT_DAY']["VALUE"].' days')) <= $newEndDate) { // если пользователь выбрал даты смотрим сегодня и подсчитываем дату конца курса
                    $table = $table.'
                                <tr style="font-size: 10px; text-align: left">
                                    <td>'.$arFields['NAME'].' <br> '.$arProps['PRICE']['VALUE'].' </td>
                                    <td>'.$arProps['HOURS']['VALUE'].'</td>
                                    <td>'.$arProps['DATE_START_NEW']["VALUE"][$i].'</td>
                                    <td>'.$arProps['DATE_START_NEW']["DESCRIPTION"][$i].'</td>
                                    <td>'.$arProps['COUNT_DAY']["VALUE"].'</td>
                                </tr>
                            ';
                }
            }
        }
    }
}

$pdf = new MYPDF('P', 'mm', 'A4', true, 'UTF-8');
$pdf->SetFont('roboto', '', 14, '', 'false'); // установка шрифта

$pdf->AddPage(); // Добавляем страницу
$pdf->SetXY(30, 30); // Установка текущей точки (в мм)
$pdf->SetDrawColor(100, 100, 0); // Установка цвета (RGB)
$pdf->SetTextColor(0, 0, 0); // Установка цвета текста (RGB)

$pdf->SetTitle('Расписание курсов PDF');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
//$pdf->SetHeaderData('/var/www/u0551855/data/www/cpkk.testck.ru/local/templates/cpreuro/assets/images/logo.png', 100, 'test'.' 001', 'test-1', array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

$pdf->SetHeaderMargin(100);
$pdf->SetFooterMargin(20);

$html = '
    <h1>Расписание курсов</h1>
    <table border="1" style="border-collapse: collapse">
        <thead>
            <tr style="font-size: 12px; text-align: center">
                <td> Курс </td>
                <td> Кол-во часов </td>
                <td> Дата начала </td>
                <td> Город </td>
                <td> Кол-во дней </td>
            </tr>
        </thead>
        <tbody>
            '.$table.'
        </tbody>
    </table>
';

$pdf->writeHTML($html, true, false, true, false, '');

$number = rand(100000, 999999999);

//ob_clean();
$pdf->Output('courses.pdf');
//$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/pdf/courses'.$number.'.pdf', 'F');

echo '/pdf/courses'.$number.'.pdf';
