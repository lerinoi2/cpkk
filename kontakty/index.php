<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты");
$APPLICATION->SetTitle("Контакты");
?>
    <div itemscope="" itemtype="https://schema.org/Organization">
        <div itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress" style="display: none;">
        </div>
    </div>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "bread",
                        Array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0"
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row kontakty">
            <div class="col-md-8 col-xs-12 kontakty__left" >
                <h1>Контакты</h1>
                <div class="fback_form">
                    <div class="location_block">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="location_info">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "standard.php",
                                            "PATH" => "/include/kontakty/cont.php"
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <ul class="list-inline social_list">
                                    <li>
                                        <a href="https://vk.com/perm_neft" target="_blank">
                                            <svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M19 38C29.4934 38 38 29.4934 38 19C38 8.50659 29.4934 0 19 0C8.50659 0 0 8.50659 0 19C0 29.4934 8.50659 38 19 38Z" fill="#007CDD"/>
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M18.2823 27.3332H19.7736C19.7736 27.3332 20.224 27.2837 20.454 27.0358C20.6657 26.8082 20.6589 26.3808 20.6589 26.3808C20.6589 26.3808 20.6298 24.3798 21.5585 24.0851C22.474 23.7949 23.6496 26.0191 24.8956 26.8743C25.8379 27.5215 26.5539 27.3796 26.5539 27.3796L29.8856 27.3332C29.8856 27.3332 31.6285 27.2258 30.8021 25.8555C30.7344 25.7434 30.3208 24.8418 28.3249 22.9891C26.2358 21.0501 26.5156 21.3637 29.0321 18.0096C30.5647 15.967 31.1774 14.7199 30.986 14.1858C30.8034 13.6771 29.6763 13.8115 29.6763 13.8115L25.9249 13.8349C25.9249 13.8349 25.6468 13.797 25.4406 13.9203C25.2391 14.0412 25.1093 14.323 25.1093 14.323C25.1093 14.323 24.5156 15.9036 23.7237 17.2479C22.0533 20.0845 21.3854 20.2342 21.1124 20.0581C20.4774 19.6476 20.6359 18.409 20.6359 17.529C20.6359 14.7802 21.0528 13.6341 19.824 13.3374C19.4162 13.2388 19.1162 13.1738 18.0733 13.1633C16.7348 13.1494 15.6019 13.1673 14.9604 13.4817C14.5337 13.6906 14.2044 14.1563 14.4049 14.1831C14.6529 14.2163 15.2144 14.3345 15.5121 14.7399C15.8966 15.2628 15.883 16.4374 15.883 16.4374C15.883 16.4374 16.1038 19.6733 15.3672 20.0753C14.8615 20.351 14.1679 19.7881 12.6786 17.2147C11.9156 15.8965 11.3394 14.4395 11.3394 14.4395C11.3394 14.4395 11.2283 14.1672 11.0302 14.0215C10.7897 13.8451 10.4538 13.7889 10.4538 13.7889L6.88901 13.8122C6.88901 13.8122 6.35388 13.8271 6.15743 14.0598C5.98267 14.2667 6.14355 14.6949 6.14355 14.6949C6.14355 14.6949 8.93438 21.2242 12.0944 24.5146C14.9919 27.5313 18.2823 27.3332 18.2823 27.3332Z" fill="white"/>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/permneftcentre/" target="_blank">
                                            <svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22.6367 19C22.6367 21.0085 21.0085 22.6367 19 22.6367C16.9915 22.6367 15.3633 21.0085 15.3633 19C15.3633 16.9915 16.9915 15.3633 19 15.3633C21.0085 15.3633 22.6367 16.9915 22.6367 19Z" fill="#007CDD"/>
                                                <path d="M27.505 12.5656C27.3302 12.0918 27.0513 11.6631 26.6889 11.3111C26.3369 10.9487 25.9084 10.6698 25.4344 10.495C25.05 10.3457 24.4725 10.168 23.4088 10.1195C22.2581 10.0671 21.9131 10.0558 19 10.0558C16.0866 10.0558 15.7416 10.0668 14.5912 10.1192C13.5275 10.168 12.9497 10.3457 12.5656 10.495C12.0916 10.6698 11.6628 10.9487 11.3111 11.3111C10.9487 11.6631 10.6698 12.0916 10.4947 12.5656C10.3454 12.95 10.1677 13.5278 10.1193 14.5915C10.0668 15.7419 10.0555 16.0869 10.0555 19.0003C10.0555 21.9134 10.0668 22.2584 10.1193 23.409C10.1677 24.4727 10.3454 25.0503 10.4947 25.4347C10.6698 25.9087 10.9484 26.3372 11.3108 26.6892C11.6628 27.0516 12.0913 27.3305 12.5653 27.5053C12.9497 27.6549 13.5275 27.8326 14.5912 27.881C15.7416 27.9335 16.0863 27.9445 18.9997 27.9445C21.9134 27.9445 22.2584 27.9335 23.4085 27.881C24.4722 27.8326 25.05 27.6549 25.4344 27.5053C26.3859 27.1382 27.138 26.3862 27.505 25.4347C27.6543 25.0503 27.832 24.4727 27.8807 23.409C27.9332 22.2584 27.9442 21.9134 27.9442 19.0003C27.9442 16.0869 27.9332 15.7419 27.8807 14.5915C27.8323 13.5278 27.6546 12.95 27.505 12.5656ZM19 24.6023C15.9057 24.6023 13.3974 22.0943 13.3974 19C13.3974 15.9057 15.9057 13.3976 19 13.3976C22.094 13.3976 24.6024 15.9057 24.6024 19C24.6024 22.0943 22.094 24.6023 19 24.6023ZM24.8239 14.4854C24.1008 14.4854 23.5146 13.8992 23.5146 13.1761C23.5146 12.4531 24.1008 11.8669 24.8239 11.8669C25.5469 11.8669 26.1331 12.4531 26.1331 13.1761C26.1328 13.8992 25.5469 14.4854 24.8239 14.4854Z" fill="#007CDD"/>
                                                <path d="M19 0C8.50819 0 0 8.50819 0 19C0 29.4918 8.50819 38 19 38C29.4918 38 38 29.4918 38 19C38 8.50819 29.4918 0 19 0ZM29.8443 23.4981C29.7916 24.6595 29.6069 25.4524 29.3373 26.1465C28.7705 27.612 27.612 28.7705 26.1465 29.3373C25.4527 29.6069 24.6595 29.7913 23.4984 29.8443C22.3349 29.8974 21.9632 29.9102 19.0003 29.9102C16.037 29.9102 15.6657 29.8974 14.5019 29.8443C13.3408 29.7913 12.5476 29.6069 11.8538 29.3373C11.1256 29.0633 10.4663 28.6339 9.92125 28.0788C9.36635 27.534 8.93698 26.8744 8.66301 26.1465C8.39339 25.4527 8.20871 24.6595 8.15594 23.4984C8.10231 22.3346 8.08984 21.963 8.08984 19C8.08984 16.037 8.10231 15.6654 8.15565 14.5019C8.20842 13.3405 8.39281 12.5476 8.66243 11.8535C8.9364 11.1256 9.36606 10.466 9.92125 9.92125C10.466 9.36606 11.1256 8.93669 11.8535 8.66272C12.5476 8.3931 13.3405 8.20871 14.5019 8.15565C15.6654 8.1026 16.037 8.08984 19 8.08984C21.963 8.08984 22.3346 8.1026 23.4981 8.15594C24.6595 8.20871 25.4524 8.3931 26.1465 8.66243C26.8744 8.9364 27.534 9.36606 28.079 9.92125C28.6339 10.4663 29.0636 11.1256 29.3373 11.8535C29.6072 12.5476 29.7916 13.3405 29.8446 14.5019C29.8977 15.6654 29.9102 16.037 29.9102 19C29.9102 21.963 29.8977 22.3346 29.8443 23.4981Z" fill="#007CDD"/>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/permneft/" target="_blank">
                                            <svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M18.9999 0C29.4939 0 38 8.50725 38 19.0001C38 29.4941 29.4939 38 18.9999 38C8.50594 38 0 29.4939 0 19.0001C0 8.50725 8.50607 0 18.9999 0Z" fill="#007CDD"/>
                                                <path d="M21.3133 13.0803H23.762V9.46312H20.8835V9.47616C17.3958 9.59969 16.681 11.5602 16.618 13.6194H16.6108V15.4256H14.2358V18.9678H16.6108V28.4631H20.19V18.9678H23.1219L23.6883 15.4256H20.1911V14.3343C20.1911 13.6384 20.6542 13.0803 21.3133 13.0803Z" fill="white"/>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <h2>Филиалы</h2>
                    <? $APPLICATION->IncludeComponent("bitrix:news.list", "filialy", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array(
                            0 => "NAME",
                            1 => "",
                        ),
                        "FILE_404" => "",
                        "FILTER_NAME" => "",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "10",
                        "IBLOCK_TYPE" => "contacts",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "20",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array(
                            0 => "ADDRESS",
                            1 => "PHONE",
                        ),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "Y",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "Y",
                        "SORT_BY1" => "SORT",
                        "SORT_BY2" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "ASC",
                        "SORT_ORDER2" => "DESC",
                        "STRICT_SECTION_CHECK" => "N",
                    ),
                        false
                    ); ?>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 kontakty__right">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "standard.php",
                        "PATH" => "/include/aside.php"
                    )
                ); ?>
            </div>
        </div>
        <div class="person_card">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "standard.php",
                    "PATH" => "/include/kontakty/text.php"
                )
            ); ?>
        </div>

        <div class="contacts__form">
            <h3 class="lowcase">Написать нам письмо</h3>

            <form action="/" method="POST" id="contact_form">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" required="" name="name" class="form-control" placeholder="Имя*">
                        </div>
                        <div class="form-group">
                            <input type="text" required="" name="phone" class="form-control"
                                   placeholder="Телефон*">
                        </div>
                        <div class="form-group">
                            <input type="email" required="" name="email" class="form-control"
                                   placeholder="Email">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <textarea class="form-control" name="text" placeholder="Текст сообщения"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group checkboxBlock ">
                    <div class="checkbox">
                        <input type="checkbox" id="accept">
                        <label for="accept">Нажимая кнопку «Отправить» я подтверждаю, что ознакомлен и согласен с обработкой своих персональных
                            данных в соответствии с политикой конфиденциальности</label>
                    </div>
                </div>
<!--                <div class="row">-->
<!--                    <div class="col-sm-8 col-xs-12">-->
<!--                        <div class="row">-->
<!--                            <div class="col-sm-6 col-xs-12">-->
<!--                                <div class="form-group">-->
<!--                                    <div class=" email-status">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-1 hidden-xs">-->
<!--                                <div class="capcha_arr">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-4 col-xs-12">-->
<!--                                <div class="capcha">-->
<!--                                    <img src="images/capcha.jpg" alt="">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="contacts__buttons">
                    <input type="submit" class="submit btn btn_gr" value="Отправить">
                    <input type="button" class="btn btn_gr btn_empty _clear" value="Сбросить">
                </div>
            </form>
        </div>



        <div class="row">
            <div class="col-xs-12">
                <div class="map_block">
                    <h2>Схема проезда:</h2>
                    <div class="map">
                        <p>
                            <a class="dg-widget-link"
                               href="https://2gis.ru/perm/firm/2252328094725690/center/56.284267902374275,58.002765681123414/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть
                                на карте Перми</a>
                        </p>
                        <div class="dg-widget-link">
                            <a href="https://2gis.ru/perm/firm/2252328094725690/photos/2252328094725690/center/56.284267902374275,58.002765681123414/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии
                                компании</a>
                        </div>
                        <div class="dg-widget-link">
                            <a href="https://2gis.ru/perm/center/56.284262,58.00276/zoom/16/routeTab/rsType/bus/to/56.284262,58.00276╎Европейский, центр профессионального развития?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти
                                проезд до Европейский, центр профессионального развития</a>
                        </div>
                        <p>
                            <script charset="utf-8" type="text/javascript"
                                    src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
                            <script charset="utf-8" type="text/javascript">
                                new DGWidgetLoader({
                                    "width": 640,
                                    "height": 600,
                                    "borderColor": "#a3a3a3",
                                    "pos": {"lat": 58.002765681123414, "lon": 56.284267902374275, "zoom": 16},
                                    "opt": {"city": "perm"},
                                    "org": [{"id": "2252328094664102"}]
                                });
                            </script>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>