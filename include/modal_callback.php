<div class="modal fade modal_custom" id="callModal" tabindex="-1" role="dialog" aria-labelledby="callModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="top text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2>Заказать звонок</h2>
                </div>
                <form method="POST" class="form" id="modal_form_callback">
                    <input type="hidden" name="action" value="callback">
                    <div class="form-group">
                        <input type="text" name="fio_call" class="form-control" required="" placeholder="Ваше имя*">
                    </div>
                    <div class="form-group">
                        <input type="tel" maxlength="14" name="phone_call" class="form-control" required="" placeholder="Телефон*">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email_call" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="">
                            <input type="checkbox" checked="">
                            <span>Согласен на обработку персональных данных</span>
                        </label>
                    </div>
                    <div class="email-status"></div>
                    <div class="form-group text-center">
                        <input type="submit" id="__submit" value="Отправить" class="btn btn_gr">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>