<div class="modal fade modal_custom" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="top text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <br>
                    <h2>Заявка</h2>
                </div>
                <form action="" method="POST" class="form" id="modal_form" data-course-form >
                    <div class="form-group">
                        <input type="text" class="form-control" name="course" placeholder="Название курса"
                               list="courses">
                    </div>

<!--                    <div class="form-group">-->
<!--                        <select class="study" placeholder="Формат обучения" value="">-->
<!--                            <option value="0">Очное обучение</option>-->
<!--                            <option value="1">Дистанционное</option>-->
<!--                        </select>-->
<!--                    </div>-->

<!--                    <div class="form-group">-->
<!--                        --><?//
//                        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
//                        $arFilter = Array("IBLOCK_ID"=>3, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
//                        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
//                        while($ob = $res->GetNextElement())
//                        {
//                            $arFields[] = $ob->GetFields();
//                        }
//                        ?>
<!--                        <select class="city" placeholder="Город" value="">-->
<!--                            --><?// foreach ($arFields as $item): ?>
<!--                                <option value="--><?//=$item['ID']?><!--">--><?//=$item['NAME']?><!--</option>-->
<!--                            --><?// endforeach; ?>
<!--                        </select>-->
<!--                    </div>-->
                    <div class="form-group">
                        <input type="text" name="fio" class="form-control" required=""
                               placeholder="Контактное лицо*">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control" required="" placeholder="Телефон*">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
						<textarea name="text" class="form-control"
                                  placeholder="Дополнительные пожелания"></textarea>
                    </div>
                    <div class="form-group">
                        <label for=""> <input type="checkbox" checked="">
                            Согласен на обработку персональных данных </label>
                    </div>
                    <div class="email-status">
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" id="submit" value="Отправить" class="btn btn_gr">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal_custom dist_form" id="orderModal2" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="top text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <br>
                    <h2>Заявка на дистанционное обучение</h2>
                </div>
                <form action="" method="POST" class="form" id="modal_form">
                    <div class="form-group">
                        <input type="text" class="form-control" name="course" placeholder="Название курса"
                               list="courses">
                    </div>
                    <div class="form-group">
                        <input type="text" name="fio" class="form-control" required=""
                               placeholder="Контактное лицо*">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control" required="" placeholder="Телефон*">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
						<textarea name="text" class="form-control"
                                  placeholder="Дополнительные пожелания"></textarea>
                    </div>
                    <div class="form-group">
                        <label for=""> <input type="checkbox" checked="">
                            Согласен на обработку персональных данных </label>
                    </div>
                    <div class="email-status">
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" id="submit" value="Отправить" class="btn btn_gr">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>