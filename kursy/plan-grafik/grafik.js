$(document).ready(function () {
    if ($(".select__items").length) {
        $(".select__items").mCustomScrollbar({
            axis:"y"
        });
    }

    $('.question__inputs .input.phone').mask("+7 (999) 999-9999");

    let dateNow = new Date();
    let year = dateNow.getFullYear();
    let mounth = dateNow.getMonth() + 1;
    let day = dateNow.getDate();
    let fullDate = day+'.'+mounth+'.'+year;

    
    $('._startDate, ._endDate').datepicker({
        language: 'ru',
        format : 'dd.mm.yyyy',
        autoclose: true,
        orientation: 'center bottom',
        startDate: '+1d',
    }).on('changeDate', function (e) {
        if ($(e.currentTarget).hasClass('_startDate')) {
            let startDate = e.currentTarget.value;
            // $('._endDate').datepicker('destroy').datepicker({
            //     language: 'ru',
            //     format : 'dd.mm.yyyy',
            //     autoclose: true,
            //     orientation: 'center bottom',
            //     startDate: startDate,
            // })
        } else if ($(e.currentTarget).hasClass('_endDate')) {
            let endDate = e.currentTarget.value;
        }
        dateCourses();
    });

    $('#all-date').on('change', function () {
        if ($(this).prop('checked')) {
            $('._startDate, ._endDate').attr('disabled', true);
            dateCourses();
        } else {
            $('._startDate, ._endDate').attr('disabled', false);
            $('.select__item').remove();
            $('.cloning').remove();
            $('.schedule__selects-items').addClass('disabled');
            $('._chooseSelect').text($('._chooseSelect').attr('data-name'));
            $('._chooseSelectCheckbox').text($('._chooseSelectCheckbox').attr('data-name'));
        }
    });

    $('#all-courses').on('change', function () {
        if ($(this).prop('checked')) {
            $('.schedule__selects .select').addClass('disabled');
        } else {
            $('.schedule__selects .select').removeClass('disabled');
        }
        $('._elements .select__item input').map(function (index,item) {
            $(item).prop('checked', true);
        })
    });

    $('#ochno').on('change', function () {
        if ($(this).prop('checked')) {
            $('.towns').show();
        }
    })
    $('#distance').on('change', function () {
        if ($(this).prop('checked')) {
            $('.towns').hide();
        };
    })

});

$('.schedule__selects-items').on('click', '._chooseSelect', function () {
    $('._chooseSelect').parent().removeClass('active');
    $('._chooseSelectCheckbox').parent().removeClass('active');

    let parent = $(this).parent();
    $(parent).toggleClass('active');


});

$('.schedule__selects-items').on('click', '._chooseSelectCheckbox', function () {
    $('._chooseSelect').parent().removeClass('active');
    let text = '';
    let parent = $(this).parent();
    if ($(parent).hasClass('active')) {
        $(parent).removeClass('active');
        let parentSelectedItems = $(parent).find('.select__items');
        let selectedItems = parentSelectedItems.find('.jq-checkbox.checked');
        let symbol = selectedItems.length > 1 ? ', ' : ''
        $(selectedItems).parent().each(function (index,item) {
            text = text + $(item).text() + symbol;
        });
        $(parent).find('.select__selected span').text(text);
    } else {
        $('._chooseSelectCheckbox').parent().removeClass('active');
        $(parent).addClass('active');

    }
    // $('._chooseSelectCheckbox').parent().removeClass('active');
});

$(document).mouseup(function (e){ // событие клика по веб-документу
    if ($(".select.active").length > 0) {
        var div = $(".select.active"); // тут указываем ID элемента
        let text = '';
        if (!div.is(e.target) && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $(div).removeClass('active');
            let parentSelectedItems = $(div).find('.select__items');
            let selectedItems = parentSelectedItems.find('.jq-checkbox.checked');
            let symbol = selectedItems.length > 1 ? ', ' : ''
            $(selectedItems).parent().each(function (index,item) {
                text = text + $(item).text() + symbol;
            });
            $(div).find('.select__selected span').text(text);
        }
    }
});

$('.schedule__selects-items').on('click', '._selectItem', function () {
    let parent = $(this).closest('.select');
    let attr = $(this).attr('data-attr');
    let selects = $(this).closest('.schedule__selects');

    $(parent).find('.select__item').removeClass('active');
    $(this).addClass('active');
    $(parent).find('.select__selected span').text($(this).text());
    $(parent).toggleClass('active');
    $(selects).find('._elements .select__item').hide();
    $(selects).find('._elements .select__item[data-attr='+attr+']').show();

    // debugger;
    let checkbox = $(this).closest('.schedule__selects').find($('._chooseSelectCheckbox'));
    $(checkbox).find('span').text($(checkbox).attr('data-name'));
    $(parent).parent().find('.jq-checkbox').removeClass('checked');




});

$('.schedule__selects-items ').on('mouseup', '.cloning .checkbox', function () {
    if ($(this).find('.jq-checkbox').hasClass('checked')) {
        $(this).find('.jq-checkbox').removeClass('checked');
    } else {
        $(this).find('.jq-checkbox').addClass('checked');
    }

    // $(this).toggleClass('checked');
});
$('._pdf').on('click', function () {
    pdfFormated();
})


$('._addCourses').on('click', function () {
    $('._forCloning').clone().appendTo('.schedule__selects-items').removeClass('_forCloning').addClass('cloning').css('display', 'block');
    if ($(".select__items").length) {
        $(".select__items").mCustomScrollbar({
            axis:"y"
        });
    }
});



function dateCourses() {
    let startDate = $('._startDate').val();
    let endDate = $('._endDate').val();
    let allDate = $('#all-date').prop('checked');
    let format = $('#ochno').prop('checked');
    let city = [];
    let codeCity = {
        id: [],
        name: []
    };
    if ($('#ochno').prop('checked')) {
        $('.cities').find('input').each(function (index, item) {
            if ($(item).prop('checked'))  {
                city.push($(item).attr('data-name'));
                codeCity.id.push($(item).attr('id'));
                codeCity.name.push($(item).attr('data-name'));
            }
        });
    };
    $('.cloning').remove();

    console.log(codeCity);

    if ((startDate !== '' && endDate !== '') || allDate ) {
        $.ajax({
            type: "POST",
            url: '/ajax/plan-grafik.php',
            data: {
                'startDate': startDate,
                'endDate': endDate,
                'allDate': allDate,
                'format': format,
                'city': city,
                'codeCity': codeCity,
            },
            success: function (response) {
                // console.log('Ответ:'+response);

                let result = JSON.parse(response);
                // console.log(result);
                
                if (result.elements.length) {
                    $('.schedule__selects-items').removeClass('disabled');
                }

                let secID = '';
                let elemID = '';
                $('.select__item').remove();
                result.sections.map(function (item, index) {
                    if (index > 0 && secID != item.id) {
                        $('._sections #mCSB_1_container').append('<div class="select__item _selectItem" data-attr="'+item.id+'">'+item.name+'</div>');
                        $('._forCloning ._sections .mCSB_container').append('<div class="select__item _selectItem" data-attr="'+item.id+'">'+item.name+'</div>');
                    } else if (index == 0) {
                        $('._sections #mCSB_1_container').append('<div class="select__item _selectItem" data-attr="'+item.id+'">'+item.name+'</div>');
                        $('._forCloning ._sections .mCSB_container').append('<div class="select__item _selectItem" data-attr="'+item.id+'">'+item.name+'</div>');
                    }
                    secID = item.id;
                });
                result.elements.map(function (item, index) {
                    if (index > 0 && elemID != item.field.ID) {
                        $('._elements #mCSB_2_container').append('' +
                            '<div class="select__item" data-id="' + item.field.ID + '" data-attr="' + item.field.IBLOCK_SECTION_ID + '">' +
                            '<div class="checkbox">' +
                            '<label>' +
                            '<input type="checkbox">' +
                            '' + item.field.NAME + '' +
                            '</label>' +
                            '</div>' +
                            '</div>')
                        $('._forCloning ._elements .mCSB_container').append('' +
                            '<div class="select__item" data-id="' + item.field.ID + '" data-attr="' + item.field.IBLOCK_SECTION_ID + '">' +
                            '<div class="checkbox">' +
                            '<label>' +
                            '<input type="checkbox">' +
                            '' + item.field.NAME + '' +
                            '</label>' +
                            '</div>' +
                            '</div>')
                    } else if (index == 0) {
                        $('._elements #mCSB_2_container').append('' +
                            '<div class="select__item" data-id="' + item.field.ID + '" data-attr="' + item.field.IBLOCK_SECTION_ID + '">' +
                            '<div class="checkbox">' +
                            '<label>' +
                            '<input type="checkbox">' +
                            '' + item.field.NAME + '' +
                            '</label>' +
                            '</div>' +
                            '</div>')
                        $('._forCloning ._elements .mCSB_container').append('' +
                            '<div class="select__item" data-id="' + item.field.ID + '" data-attr="' + item.field.IBLOCK_SECTION_ID + '">' +
                            '<div class="checkbox">' +
                            '<label>' +
                            '<input type="checkbox">' +
                            '' + item.field.NAME + '' +
                            '</label>' +
                            '</div>' +
                            '</div>')
                    }
                    elemID = item.field.ID;
                });

                setTimeout(function () {
                    $('input[type="checkbox"]').styler();
                },500)
            },
            error: function (response) {
                alert("Ошибка отправки");
            }
        });
    }
}

function pdfFormated() {
    let startDate = $('._startDate').val();
    let endDate = $('._endDate').val();
    let allDate = $('#all-date').prop('checked');
    let format = $('#ochno').prop('checked');
    let city = [];
    let codeCity = {
        id: [],
        name: []
    };
    let napravlenie = [];
    let courses = [];
    let allCourses = $('#all-courses').prop('checked');

    $('._sections .select__item.active').map(function (index,item) {
        napravlenie.push($(item).attr('data-attr'));
    });
    $('._elements .select__item input').map(function (index,item) {
        if ($(item).prop('checked')) {
            courses.push($(item).closest('label').text());
        }
    })

    if ($('#ochno').prop('checked')) {
        $('.cities').find('input').each(function (index, item) {
            if ($(item).prop('checked'))  {
                city.push($(item).attr('data-name'));
                codeCity.id.push($(item).attr('id'));
                codeCity.name.push($(item).attr('data-name'));
            }
        });
    };
    if ((startDate !== '' && endDate !== '') || allDate ) {
        $.ajax({
            type: "POST",
            url: '/ajax/dompdf.php',
            data: {
                'startDate': startDate,
                'endDate': endDate,
                'allDate': allDate,
                'format': format,
                'city': city,
                'codeCity': codeCity,
                'napravlenie': napravlenie,
                'courses': courses,
                'allCourses': allCourses
            },
            success: function (response) {
                // console.log(response);
                // window.location.href = '/pdf/test.pdf';
                window.location.href = response;
            },
            error: function (response) {
                alert("Ошибка отправки");
            }
        });
    }


}

