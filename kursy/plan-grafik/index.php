<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetPageProperty('title', 'План-график');
$APPLICATION->SetTitle('План-график');
?>

<script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/local/templates/cpreuro/assets/libs/customscroll/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="/local/templates/cpreuro/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="./plan.css">

<div class="schedule">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "bread",
                    Array(
                        "PATH" => "",
                        "SITE_ID" => "s1",
                        "START_FROM" => "0"
                    )
                ); ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="schedule__container">
            <div class="schedule__top">
                <div class="schedule__title">Создайте свое удобное расписание</div>
                <div class="schedule__text">Создайте свой план обучения в несколько простых шагов - выберите время, направление и формат обучения. Конструктор сформирует для вас pdf-расписание, с которым удобно планировать обучение.</div>
            </div>
            <div class="schedule__form">
                <form action="">

                    <!-- Формат обучения -->
                    <div class="schedule__form-item">
                        <div class="schedule__form-title"><span>01.</span>Формат</div>
                        <div class="schedule__form-info">
                            <div class="schedule__inputs-title">Выберите формат обучения</div>
                            <div class="schedule__inputs checkboxes">
                                <div class="radioButton">
                                    <input type="radio" id="ochno" name="format" checked>
                                    <label for="ochno">Очное</label>
                                </div>
                                <div class="radioButton">
                                    <input type="radio" id="distance" name="format">
                                    <label for="distance">Дистанционное</label>
                                </div>
                            </div>
                        </div>

                        <div class="schedule__form-info towns">
                            <div class="schedule__inputs-title">Выберите город</div>
                            <div class="schedule__inputs checkboxes cities">
                                <?
                                    $city_list = CIBlockElement::GetList(
                                        Array("SORT" => "ASC"),
                                        Array('IBLOCK_ID' => 3, 'GLOBAL_ACTIVE' => 'Y'),
                                        false,
                                        Array(),
                                        Array()
                                    );
                                while($city_res = $city_list->GetNext()) {
                                    if ($city_res['NAME'] === 'Пермь') { ?>
                                        <div class="checkbox">
                                            <input type="checkbox" name="town" id="<?=$city_res['ID']?>" data-name="<?=$city_res['NAME']?>" checked>
                                            <label for="<?=$city_res['ID']?>"><?=$city_res['NAME']?></label>
                                        </div>
                                    <? } else {?>
                                    <div class="checkbox">
                                        <input type="checkbox" name="town" id="<?=$city_res['ID']?>" data-name="<?=$city_res['NAME']?>">
                                        <label for="<?=$city_res['ID']?>"><?=$city_res['NAME']?></label>
                                    </div>

                                <?}}
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- end Формат обучения -->

                    <!-- Дата обучения -->
                    <div class="schedule__form-item">
                        <div class="schedule__form-title"><span>02.</span>Дата</div>
                        <div class="schedule__form-info">
                            <div class="schedule__inputs-title">Выберите дату</div>
                            <div class="schedule__inputs">
                                <div class="schedule__dates">
                                    <input type="text" class="input form-control _startDate" placeholder="С" readonly>
                                    <span class="tire"> — </span> 
                                    <input type="text" class="input form-control _endDate" placeholder="ПО" readonly>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" id="all-date">
                                    <label for="all-date">Все даты</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end Дата обучения -->

                    <!-- Направление/курс -->
                    <div class="schedule__form-item">
                        <div class="schedule__form-title"><span>03.</span>Курс</div>
                        <div class="schedule__form-info">
                            <div class="schedule__inputs-title">Выберите курс</div>
                            <div class="schedule__selects-items disabled">
                                <div class="schedule__selects">
                                    <div class="select">
                                        <div class="select__selected _chooseSelect" data-name="Направление обучения"><span>Направление обучения</span></div>
                                        <div class="select__items _sections">
                                        </div>
                                        <div class="error-message">Выберите направление</div>
                                    </div>

                                    <div class="select">
                                        <div class="select__selected _chooseSelectCheckbox" data-name="Курс"><span>Курс</span></div>
                                        <div class="select__items _elements">
                                        </div>
                                        <div class="error-message">Выберите курс</div>
                                    </div>
                                </div>
                            </div>
                            <div class="schedule__selects _forCloning" style="display: none">
                                <div class="select">
                                    <div class="select__selected _chooseSelect" data-name="Направление обучения"><span>Направление обучения</span></div>
                                    <div class="select__items _sections"></div>
                                    <div class="error-message">Выберите направление</div>
                                </div>
                                <div class="select">
                                    <div class="select__selected _chooseSelectCheckbox" data-name="Курс"><span>Курс</span></div>
                                    <div class="select__items _elements"></div>
                                    <div class="error-message">Выберите курс</div>
                                </div>
                            </div>

                            <div class="schedule__add"><span class="_addCourses">+ Добавить еще курс</span></div>
                            <div class="checkbox">
                                <input type="checkbox" id="all-courses">
                                <label for="all-courses">Все Курсы</label>
                            </div>
                        </div>
                    </div>
                    <!-- end Направление/курс -->


                    <div class="schedule__form-button _pdf">Скачать План-график</div>
                </form>
            </div>


            <div class="schedule__info">
                <div class="schedule__info-title">Учебный центр <br> «Пермь-Нефть» это:</div>
                <div class="schedule__info-items">
                    <div class="schedule__info-item">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/kursy/plan-grafik/1_block.php"
                            )
                        ); ?>
                    </div>
                    <div class="schedule__info-item">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/kursy/plan-grafik/2_block.php"
                            )
                        ); ?>
                    </div>
                    <div class="schedule__info-item">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/kursy/plan-grafik/3_block.php"
                            )
                        ); ?>
                    </div>
                    <div class="schedule__info-item">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/kursy/plan-grafik/4_block.php"
                            )
                        ); ?>
                    </div>
                </div>
            </div>
<!--            <div class="schedule__question">-->
<!--                <div class="question__form">-->
<!--                    <div class="question__title">У Вас остались вопросы?</div>-->
<!--                    <div class="question__text">Расскажем Вам всё о наших услугах и ответим на все вопросы.</div>-->
<!--                    <form action="">-->
<!--                        <div class="question__inputs">-->
<!--                            <input type="text" class="input" placeholder="Ваше имя">-->
<!--                            <input type="text" class="input phone" name="tel" placeholder="Телефон *">-->
<!--                        </div>-->
<!--                        <div class="question__button">Отправить</div>-->
<!--                    </form>-->
<!--                    <div class="question__checkbox">-->
<!--                        <div class="checkbox">-->
<!--                            <input type="checkbox" id="personal-data">-->
<!--                            <label for="personal-data">Согласен на обработку персональных данных </label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <div class="container">
                <form id="course_questions_form" action="javascript:alert('Form error!');" type="post">
                    <div class="kursi-capture-3">
                        <div class="kursi-capture-3__img"><img src="/local/templates/cpreuro/assets/images/kursi-capture-3-img.png">
                        </div>
                        <div class="section-title">У Вас остались вопросы?</div>
                        <div class="kursi-capture-3__subtitle">Расскажем Вам всё о наших услугах и ответим на все вопросы.</div>
                        <div class="kursi-capture-3__row">
                            <input type="hidden" name="course" value="План-график">
                            <input type="hidden" name="action" value="callback">
                            <div class="kursi-capture-3__textfield">
                                <input class="n-textfield" type="text" name="name" placeholder="Ваше имя *" required="">
                            </div>
                            <div class="kursi-capture-3__textfield">
                                <input class="n-textfield" type="text" name="phone" placeholder="Телефон *"
                                       pattern="(\+?\d[- .]*){7,13}"
                                       title="Международный, государственный или местный телефонный номер" required="">
                            </div>
                            <div class="kursi-capture-3__btn">
                                <button class="btn-md btn-blue">Отправить</button>
                            </div>
                        </div>
                        <div class="kursi-capture-3__checkbox">
                            <label class="n-checkbox" for="checkbox-2">
                                <input type="checkbox" id="checkbox-2" class="styler-disabled">
                                <span> </span>
                            </label>
                            <label class="n-ckeckbox-label" for="checkbox-2">Согласен на обработку персональных данных</label>
                        </div>
                    </div>
                    <input type="hidden" name="form" value="course_questions_form">
                </form>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="/local/templates/cpreuro/assets/libs/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/local/templates/cpreuro/assets/libs/customscroll/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script type="text/javascript" src="/local/templates/cpreuro/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/local/templates/cpreuro/assets/libs/bootstrap-datepicker/locales/bootstrap-datepicker.ru.js" charset="UTF-8"></script>

<script type="text/javascript" src="./grafik.js"></script>


<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
